﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Алексей
 * Дата: 02.05.2014
 * Время: 16:10
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
namespace БД_для_ОТ
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabIntervals = new System.Windows.Forms.TabPage();
			this.btnToExcelIntervals = new System.Windows.Forms.Button();
			this.btnFindPlace = new System.Windows.Forms.Button();
			this.btnFindManInfo = new System.Windows.Forms.Button();
			this.pnlIntervals = new System.Windows.Forms.Panel();
			this.label10 = new System.Windows.Forms.Label();
			this.lblAddIntervalStatus = new System.Windows.Forms.Label();
			this.tbIntervalsComment = new System.Windows.Forms.TextBox();
			this.cBoxIntervalsIsTrezv = new System.Windows.Forms.CheckBox();
			this.cbIntervalPlace = new System.Windows.Forms.ComboBox();
			this.cbIntervalFIO = new System.Windows.Forms.ComboBox();
			this.btnAddInterval = new System.Windows.Forms.Button();
			this.dtpDateBegin = new System.Windows.Forms.DateTimePicker();
			this.cbIntervalType = new System.Windows.Forms.ComboBox();
			this.dtpDateEnd = new System.Windows.Forms.DateTimePicker();
			this.label20 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.lblIntervalsStatus = new System.Windows.Forms.Label();
			this.dgvIntervals = new System.Windows.Forms.DataGridView();
			this.label18 = new System.Windows.Forms.Label();
			this.dtpNearVowFrom = new System.Windows.Forms.DateTimePicker();
			this.dtpNearVowTo = new System.Windows.Forms.DateTimePicker();
			this.btnGetNearVow = new System.Windows.Forms.Button();
			this.label17 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.btnGetInVow = new System.Windows.Forms.Button();
			this.dtpInVow = new System.Windows.Forms.DateTimePicker();
			this.label19 = new System.Windows.Forms.Label();
			this.btnGetManVows = new System.Windows.Forms.Button();
			this.btnGetAllIntervals = new System.Windows.Forms.Button();
			this.tabInfo = new System.Windows.Forms.TabPage();
			this.cbMonthTo = new System.Windows.Forms.ComboBox();
			this.cbDayTo = new System.Windows.Forms.ComboBox();
			this.cbDayFrom = new System.Windows.Forms.ComboBox();
			this.btnToExcelManInfo = new System.Windows.Forms.Button();
			this.btnGetBirthday = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.pnlInfo = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.lblAddManStatus = new System.Windows.Forms.Label();
			this.tbFName = new System.Windows.Forms.TextBox();
			this.tbName = new System.Windows.Forms.TextBox();
			this.tbOName = new System.Windows.Forms.TextBox();
			this.tbAddress = new System.Windows.Forms.TextBox();
			this.tbTel = new System.Windows.Forms.TextBox();
			this.btnAddMan = new System.Windows.Forms.Button();
			this.tbEmail = new System.Windows.Forms.TextBox();
			this.tbManComment = new System.Windows.Forms.TextBox();
			this.dtpBirthday = new System.Windows.Forms.DateTimePicker();
			this.tbBirthday = new System.Windows.Forms.TextBox();
			this.btnGetAllManInfo = new System.Windows.Forms.Button();
			this.lblManInfoStatus = new System.Windows.Forms.Label();
			this.dgvManInfo = new System.Windows.Forms.DataGridView();
			this.cbMonthFrom = new System.Windows.Forms.ComboBox();
			this.tabAchievements = new System.Windows.Forms.TabPage();
			this.panel6 = new System.Windows.Forms.Panel();
			this.label28 = new System.Windows.Forms.Label();
			this.lblAddAchievementStatus = new System.Windows.Forms.Label();
			this.tbAchievementComment = new System.Windows.Forms.TextBox();
			this.cbAchievementName = new System.Windows.Forms.ComboBox();
			this.cbAchievementFIO = new System.Windows.Forms.ComboBox();
			this.btnAddAchievement = new System.Windows.Forms.Button();
			this.dtpAchievement = new System.Windows.Forms.DateTimePicker();
			this.label30 = new System.Windows.Forms.Label();
			this.label31 = new System.Windows.Forms.Label();
			this.label33 = new System.Windows.Forms.Label();
			this.label35 = new System.Windows.Forms.Label();
			this.dgvAchievements = new System.Windows.Forms.DataGridView();
			this.label27 = new System.Windows.Forms.Label();
			this.tabPlaces = new System.Windows.Forms.TabPage();
			this.btnToExcelPlaces = new System.Windows.Forms.Button();
			this.pnlPlaces = new System.Windows.Forms.Panel();
			this.tbPlace = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.lblAddPlaceStatus = new System.Windows.Forms.Label();
			this.tbPlaceComment = new System.Windows.Forms.TextBox();
			this.btnAddPlace = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.dgvPlaces = new System.Windows.Forms.DataGridView();
			this.tabSettings = new System.Windows.Forms.TabPage();
			this.panel5 = new System.Windows.Forms.Panel();
			this.dgvAchieveNames = new System.Windows.Forms.DataGridView();
			this.label26 = new System.Windows.Forms.Label();
			this.label22 = new System.Windows.Forms.Label();
			this.panel4 = new System.Windows.Forms.Panel();
			this.lblSyncStatus = new System.Windows.Forms.Label();
			this.btnSyncSave = new System.Windows.Forms.Button();
			this.btnSyncPathBrowse = new System.Windows.Forms.Button();
			this.btnSyncOpenBrowse = new System.Windows.Forms.Button();
			this.lblSyncFileCountStatus = new System.Windows.Forms.Label();
			this.tbSyncPath = new System.Windows.Forms.TextBox();
			this.label25 = new System.Windows.Forms.Label();
			this.label24 = new System.Windows.Forms.Label();
			this.label23 = new System.Windows.Forms.Label();
			this.panel3 = new System.Windows.Forms.Panel();
			this.btnBackupBrowse = new System.Windows.Forms.Button();
			this.lblBackupStatus = new System.Windows.Forms.Label();
			this.btnBackup = new System.Windows.Forms.Button();
			this.tbBackupPath = new System.Windows.Forms.TextBox();
			this.label21 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.lblSettingsInfo = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.label15 = new System.Windows.Forms.Label();
			this.btnSaveSettings = new System.Windows.Forms.Button();
			this.tbDBPath = new System.Windows.Forms.TextBox();
			this.btnDBPathBrowse = new System.Windows.Forms.Button();
			this.lblSaveSettingsStatus = new System.Windows.Forms.Label();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.tabControl1.SuspendLayout();
			this.tabIntervals.SuspendLayout();
			this.pnlIntervals.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvIntervals)).BeginInit();
			this.tabInfo.SuspendLayout();
			this.pnlInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvManInfo)).BeginInit();
			this.tabAchievements.SuspendLayout();
			this.panel6.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvAchievements)).BeginInit();
			this.tabPlaces.SuspendLayout();
			this.pnlPlaces.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvPlaces)).BeginInit();
			this.tabSettings.SuspendLayout();
			this.panel5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvAchieveNames)).BeginInit();
			this.panel4.SuspendLayout();
			this.panel3.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabIntervals);
			this.tabControl1.Controls.Add(this.tabInfo);
			this.tabControl1.Controls.Add(this.tabAchievements);
			this.tabControl1.Controls.Add(this.tabPlaces);
			this.tabControl1.Controls.Add(this.tabSettings);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(964, 544);
			this.tabControl1.TabIndex = 0;
			this.tabControl1.Tag = "";
			// 
			// tabIntervals
			// 
			this.tabIntervals.Controls.Add(this.btnToExcelIntervals);
			this.tabIntervals.Controls.Add(this.btnFindPlace);
			this.tabIntervals.Controls.Add(this.btnFindManInfo);
			this.tabIntervals.Controls.Add(this.pnlIntervals);
			this.tabIntervals.Controls.Add(this.lblIntervalsStatus);
			this.tabIntervals.Controls.Add(this.dgvIntervals);
			this.tabIntervals.Controls.Add(this.label18);
			this.tabIntervals.Controls.Add(this.dtpNearVowFrom);
			this.tabIntervals.Controls.Add(this.dtpNearVowTo);
			this.tabIntervals.Controls.Add(this.btnGetNearVow);
			this.tabIntervals.Controls.Add(this.label17);
			this.tabIntervals.Controls.Add(this.label16);
			this.tabIntervals.Controls.Add(this.btnGetInVow);
			this.tabIntervals.Controls.Add(this.dtpInVow);
			this.tabIntervals.Controls.Add(this.label19);
			this.tabIntervals.Controls.Add(this.btnGetManVows);
			this.tabIntervals.Controls.Add(this.btnGetAllIntervals);
			this.tabIntervals.Location = new System.Drawing.Point(4, 22);
			this.tabIntervals.Name = "tabIntervals";
			this.tabIntervals.Size = new System.Drawing.Size(956, 518);
			this.tabIntervals.TabIndex = 3;
			this.tabIntervals.Text = "Обеты и промежутки между ними";
			this.tabIntervals.UseVisualStyleBackColor = true;
			// 
			// btnToExcelIntervals
			// 
			this.btnToExcelIntervals.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnToExcelIntervals.Location = new System.Drawing.Point(803, 94);
			this.btnToExcelIntervals.Name = "btnToExcelIntervals";
			this.btnToExcelIntervals.Size = new System.Drawing.Size(145, 23);
			this.btnToExcelIntervals.TabIndex = 69;
			this.btnToExcelIntervals.Text = "Экспорт в Excel";
			this.btnToExcelIntervals.UseVisualStyleBackColor = true;
			this.btnToExcelIntervals.Click += new System.EventHandler(this.BtnToExcelIntervalsClick);
			// 
			// btnFindPlace
			// 
			this.btnFindPlace.Location = new System.Drawing.Point(460, 94);
			this.btnFindPlace.Name = "btnFindPlace";
			this.btnFindPlace.Size = new System.Drawing.Size(337, 23);
			this.btnFindPlace.TabIndex = 68;
			this.btnFindPlace.Text = "Найти информацию о месте принятия обета выбранного лица";
			this.btnFindPlace.UseVisualStyleBackColor = true;
			this.btnFindPlace.Click += new System.EventHandler(this.BtnFindPlaceClick);
			// 
			// btnFindManInfo
			// 
			this.btnFindManInfo.Location = new System.Drawing.Point(240, 94);
			this.btnFindManInfo.Name = "btnFindManInfo";
			this.btnFindManInfo.Size = new System.Drawing.Size(214, 23);
			this.btnFindManInfo.TabIndex = 67;
			this.btnFindManInfo.Text = "Найти информацию о выбранном лице";
			this.btnFindManInfo.UseVisualStyleBackColor = true;
			this.btnFindManInfo.Click += new System.EventHandler(this.BtnFindManInfoClick);
			// 
			// pnlIntervals
			// 
			this.pnlIntervals.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.pnlIntervals.Controls.Add(this.label10);
			this.pnlIntervals.Controls.Add(this.lblAddIntervalStatus);
			this.pnlIntervals.Controls.Add(this.tbIntervalsComment);
			this.pnlIntervals.Controls.Add(this.cBoxIntervalsIsTrezv);
			this.pnlIntervals.Controls.Add(this.cbIntervalPlace);
			this.pnlIntervals.Controls.Add(this.cbIntervalFIO);
			this.pnlIntervals.Controls.Add(this.btnAddInterval);
			this.pnlIntervals.Controls.Add(this.dtpDateBegin);
			this.pnlIntervals.Controls.Add(this.cbIntervalType);
			this.pnlIntervals.Controls.Add(this.dtpDateEnd);
			this.pnlIntervals.Controls.Add(this.label20);
			this.pnlIntervals.Controls.Add(this.label5);
			this.pnlIntervals.Controls.Add(this.label14);
			this.pnlIntervals.Controls.Add(this.label11);
			this.pnlIntervals.Controls.Add(this.label12);
			this.pnlIntervals.Controls.Add(this.label13);
			this.pnlIntervals.Location = new System.Drawing.Point(8, 421);
			this.pnlIntervals.Name = "pnlIntervals";
			this.pnlIntervals.Size = new System.Drawing.Size(940, 89);
			this.pnlIntervals.TabIndex = 45;
			// 
			// label10
			// 
			this.label10.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label10.Location = new System.Drawing.Point(3, 0);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(194, 16);
			this.label10.TabIndex = 14;
			this.label10.Text = "Добавить строку в таблицу:";
			// 
			// lblAddIntervalStatus
			// 
			this.lblAddIntervalStatus.Location = new System.Drawing.Point(30, 57);
			this.lblAddIntervalStatus.Name = "lblAddIntervalStatus";
			this.lblAddIntervalStatus.Size = new System.Drawing.Size(743, 23);
			this.lblAddIntervalStatus.TabIndex = 44;
			// 
			// tbIntervalsComment
			// 
			this.tbIntervalsComment.Location = new System.Drawing.Point(744, 34);
			this.tbIntervalsComment.Name = "tbIntervalsComment";
			this.tbIntervalsComment.Size = new System.Drawing.Size(187, 20);
			this.tbIntervalsComment.TabIndex = 21;
			this.tbIntervalsComment.KeyUp += new System.Windows.Forms.KeyEventHandler(this.AddIntervalCtrlsKeyUp);
			// 
			// cBoxIntervalsIsTrezv
			// 
			this.cBoxIntervalsIsTrezv.Checked = true;
			this.cBoxIntervalsIsTrezv.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cBoxIntervalsIsTrezv.Location = new System.Drawing.Point(623, 34);
			this.cBoxIntervalsIsTrezv.Name = "cBoxIntervalsIsTrezv";
			this.cBoxIntervalsIsTrezv.Size = new System.Drawing.Size(115, 24);
			this.cBoxIntervalsIsTrezv.TabIndex = 20;
			this.cBoxIntervalsIsTrezv.Text = "Проведён трезво";
			this.cBoxIntervalsIsTrezv.UseVisualStyleBackColor = true;
			this.cBoxIntervalsIsTrezv.KeyUp += new System.Windows.Forms.KeyEventHandler(this.AddIntervalCtrlsKeyUp);
			// 
			// cbIntervalPlace
			// 
			this.cbIntervalPlace.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
			this.cbIntervalPlace.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
			this.cbIntervalPlace.FormattingEnabled = true;
			this.cbIntervalPlace.Location = new System.Drawing.Point(284, 36);
			this.cbIntervalPlace.Name = "cbIntervalPlace";
			this.cbIntervalPlace.Size = new System.Drawing.Size(121, 21);
			this.cbIntervalPlace.TabIndex = 17;
			this.cbIntervalPlace.KeyUp += new System.Windows.Forms.KeyEventHandler(this.AddIntervalCtrlsKeyUp);
			// 
			// cbIntervalFIO
			// 
			this.cbIntervalFIO.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
			this.cbIntervalFIO.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
			this.cbIntervalFIO.FormattingEnabled = true;
			this.cbIntervalFIO.Location = new System.Drawing.Point(30, 36);
			this.cbIntervalFIO.Name = "cbIntervalFIO";
			this.cbIntervalFIO.Size = new System.Drawing.Size(121, 21);
			this.cbIntervalFIO.TabIndex = 15;
			this.cbIntervalFIO.KeyUp += new System.Windows.Forms.KeyEventHandler(this.AddIntervalCtrlsKeyUp);
			this.cbIntervalFIO.Leave += new System.EventHandler(this.CbIntervalFIOLeave);
			// 
			// btnAddInterval
			// 
			this.btnAddInterval.Location = new System.Drawing.Point(779, 57);
			this.btnAddInterval.Name = "btnAddInterval";
			this.btnAddInterval.Size = new System.Drawing.Size(152, 23);
			this.btnAddInterval.TabIndex = 22;
			this.btnAddInterval.Text = "Добавить";
			this.btnAddInterval.UseVisualStyleBackColor = true;
			this.btnAddInterval.Click += new System.EventHandler(this.BtnAddIntervalClick);
			// 
			// dtpDateBegin
			// 
			this.dtpDateBegin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpDateBegin.Location = new System.Drawing.Point(411, 36);
			this.dtpDateBegin.Name = "dtpDateBegin";
			this.dtpDateBegin.Size = new System.Drawing.Size(100, 20);
			this.dtpDateBegin.TabIndex = 18;
			this.dtpDateBegin.KeyUp += new System.Windows.Forms.KeyEventHandler(this.AddIntervalCtrlsKeyUp);
			// 
			// cbIntervalType
			// 
			this.cbIntervalType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbIntervalType.FormattingEnabled = true;
			this.cbIntervalType.Items.AddRange(new object[] {
									"Обет",
									"Промежуток между обетами"});
			this.cbIntervalType.Location = new System.Drawing.Point(157, 36);
			this.cbIntervalType.Name = "cbIntervalType";
			this.cbIntervalType.Size = new System.Drawing.Size(121, 21);
			this.cbIntervalType.TabIndex = 16;
			this.cbIntervalType.KeyUp += new System.Windows.Forms.KeyEventHandler(this.AddIntervalCtrlsKeyUp);
			// 
			// dtpDateEnd
			// 
			this.dtpDateEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpDateEnd.Location = new System.Drawing.Point(517, 36);
			this.dtpDateEnd.Name = "dtpDateEnd";
			this.dtpDateEnd.ShowCheckBox = true;
			this.dtpDateEnd.Size = new System.Drawing.Size(100, 20);
			this.dtpDateEnd.TabIndex = 19;
			this.dtpDateEnd.KeyUp += new System.Windows.Forms.KeyEventHandler(this.AddIntervalCtrlsKeyUp);
			// 
			// label20
			// 
			this.label20.Location = new System.Drawing.Point(744, 19);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(84, 19);
			this.label20.TabIndex = 43;
			this.label20.Text = "Комментарий:";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(284, 19);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(100, 19);
			this.label5.TabIndex = 40;
			this.label5.Text = "Где принят:";
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(517, 19);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(100, 19);
			this.label14.TabIndex = 23;
			this.label14.Text = "Дата конца:";
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(30, 19);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(100, 19);
			this.label11.TabIndex = 20;
			this.label11.Text = "ФИО:";
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(157, 19);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(100, 19);
			this.label12.TabIndex = 21;
			this.label12.Text = "Тип промежутка:";
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(411, 19);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(100, 16);
			this.label13.TabIndex = 22;
			this.label13.Text = "Дата начала:";
			// 
			// lblIntervalsStatus
			// 
			this.lblIntervalsStatus.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lblIntervalsStatus.Location = new System.Drawing.Point(8, 7);
			this.lblIntervalsStatus.Name = "lblIntervalsStatus";
			this.lblIntervalsStatus.Size = new System.Drawing.Size(704, 17);
			this.lblIntervalsStatus.TabIndex = 38;
			this.lblIntervalsStatus.Text = "Здесь отображается ФИО человека и информация о последнем, данном им, обете";
			// 
			// dgvIntervals
			// 
			this.dgvIntervals.AllowUserToAddRows = false;
			this.dgvIntervals.AllowUserToDeleteRows = false;
			this.dgvIntervals.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.dgvIntervals.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvIntervals.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
			this.dgvIntervals.Location = new System.Drawing.Point(8, 123);
			this.dgvIntervals.Name = "dgvIntervals";
			this.dgvIntervals.Size = new System.Drawing.Size(940, 292);
			this.dgvIntervals.TabIndex = 26;
			this.dgvIntervals.Tag = "3";
			this.dgvIntervals.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvCellClick);
			// 
			// label18
			// 
			this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label18.Location = new System.Drawing.Point(403, 64);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(23, 19);
			this.label18.TabIndex = 62;
			this.label18.Text = "до";
			// 
			// dtpNearVowFrom
			// 
			this.dtpNearVowFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpNearVowFrom.Location = new System.Drawing.Point(294, 63);
			this.dtpNearVowFrom.Name = "dtpNearVowFrom";
			this.dtpNearVowFrom.Size = new System.Drawing.Size(100, 20);
			this.dtpNearVowFrom.TabIndex = 63;
			// 
			// dtpNearVowTo
			// 
			this.dtpNearVowTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpNearVowTo.Location = new System.Drawing.Point(432, 63);
			this.dtpNearVowTo.Name = "dtpNearVowTo";
			this.dtpNearVowTo.Size = new System.Drawing.Size(100, 20);
			this.dtpNearVowTo.TabIndex = 64;
			// 
			// btnGetNearVow
			// 
			this.btnGetNearVow.Location = new System.Drawing.Point(538, 62);
			this.btnGetNearVow.Name = "btnGetNearVow";
			this.btnGetNearVow.Size = new System.Drawing.Size(85, 22);
			this.btnGetNearVow.TabIndex = 65;
			this.btnGetNearVow.Text = "отобразить";
			this.btnGetNearVow.UseVisualStyleBackColor = true;
			this.btnGetNearVow.Click += new System.EventHandler(this.BtnGetNearVowClick);
			// 
			// label17
			// 
			this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label17.Location = new System.Drawing.Point(267, 64);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(31, 20);
			this.label17.TabIndex = 61;
			this.label17.Text = "От";
			// 
			// label16
			// 
			this.label16.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label16.Location = new System.Drawing.Point(267, 41);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(379, 18);
			this.label16.TabIndex = 60;
			this.label16.Text = "Отобразить обеты, заканчивающиеся в заданном диапазоне:";
			// 
			// btnGetInVow
			// 
			this.btnGetInVow.Location = new System.Drawing.Point(131, 64);
			this.btnGetInVow.Name = "btnGetInVow";
			this.btnGetInVow.Size = new System.Drawing.Size(85, 22);
			this.btnGetInVow.TabIndex = 59;
			this.btnGetInVow.Text = "отобразить";
			this.btnGetInVow.UseVisualStyleBackColor = true;
			this.btnGetInVow.Click += new System.EventHandler(this.BtnGetInVowClick);
			// 
			// dtpInVow
			// 
			this.dtpInVow.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpInVow.Location = new System.Drawing.Point(25, 65);
			this.dtpInVow.Name = "dtpInVow";
			this.dtpInVow.Size = new System.Drawing.Size(100, 20);
			this.dtpInVow.TabIndex = 58;
			// 
			// label19
			// 
			this.label19.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label19.Location = new System.Drawing.Point(8, 41);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(253, 23);
			this.label19.TabIndex = 57;
			this.label19.Text = "Отобразить действующие обеты на дату:";
			// 
			// btnGetManVows
			// 
			this.btnGetManVows.Location = new System.Drawing.Point(8, 94);
			this.btnGetManVows.Name = "btnGetManVows";
			this.btnGetManVows.Size = new System.Drawing.Size(226, 23);
			this.btnGetManVows.TabIndex = 66;
			this.btnGetManVows.Text = "Отобразить все обеты выбранного лица";
			this.btnGetManVows.UseVisualStyleBackColor = true;
			this.btnGetManVows.Click += new System.EventHandler(this.BtnGetManVowsClick);
			// 
			// btnGetAllIntervals
			// 
			this.btnGetAllIntervals.Location = new System.Drawing.Point(8, 94);
			this.btnGetAllIntervals.Name = "btnGetAllIntervals";
			this.btnGetAllIntervals.Size = new System.Drawing.Size(226, 23);
			this.btnGetAllIntervals.TabIndex = 46;
			this.btnGetAllIntervals.Text = "Отобразить информацию обо всех людях";
			this.btnGetAllIntervals.UseVisualStyleBackColor = true;
			this.btnGetAllIntervals.Visible = false;
			this.btnGetAllIntervals.Click += new System.EventHandler(this.BtnGetAllIntervalsClick);
			// 
			// tabInfo
			// 
			this.tabInfo.Controls.Add(this.cbMonthTo);
			this.tabInfo.Controls.Add(this.cbDayTo);
			this.tabInfo.Controls.Add(this.cbDayFrom);
			this.tabInfo.Controls.Add(this.btnToExcelManInfo);
			this.tabInfo.Controls.Add(this.btnGetBirthday);
			this.tabInfo.Controls.Add(this.label6);
			this.tabInfo.Controls.Add(this.label7);
			this.tabInfo.Controls.Add(this.label8);
			this.tabInfo.Controls.Add(this.pnlInfo);
			this.tabInfo.Controls.Add(this.btnGetAllManInfo);
			this.tabInfo.Controls.Add(this.lblManInfoStatus);
			this.tabInfo.Controls.Add(this.dgvManInfo);
			this.tabInfo.Controls.Add(this.cbMonthFrom);
			this.tabInfo.Location = new System.Drawing.Point(4, 22);
			this.tabInfo.Name = "tabInfo";
			this.tabInfo.Padding = new System.Windows.Forms.Padding(3);
			this.tabInfo.Size = new System.Drawing.Size(956, 518);
			this.tabInfo.TabIndex = 1;
			this.tabInfo.Text = "Информация о людях";
			this.tabInfo.UseVisualStyleBackColor = true;
			// 
			// cbMonthTo
			// 
			this.cbMonthTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbMonthTo.FormattingEnabled = true;
			this.cbMonthTo.Location = new System.Drawing.Point(240, 67);
			this.cbMonthTo.Name = "cbMonthTo";
			this.cbMonthTo.Size = new System.Drawing.Size(68, 21);
			this.cbMonthTo.TabIndex = 76;
			// 
			// cbDayTo
			// 
			this.cbDayTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbDayTo.FormattingEnabled = true;
			this.cbDayTo.Location = new System.Drawing.Point(195, 67);
			this.cbDayTo.Name = "cbDayTo";
			this.cbDayTo.Size = new System.Drawing.Size(39, 21);
			this.cbDayTo.TabIndex = 75;
			// 
			// cbDayFrom
			// 
			this.cbDayFrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbDayFrom.FormattingEnabled = true;
			this.cbDayFrom.Location = new System.Drawing.Point(51, 67);
			this.cbDayFrom.Name = "cbDayFrom";
			this.cbDayFrom.Size = new System.Drawing.Size(39, 21);
			this.cbDayFrom.TabIndex = 73;
			// 
			// btnToExcelManInfo
			// 
			this.btnToExcelManInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnToExcelManInfo.Location = new System.Drawing.Point(803, 94);
			this.btnToExcelManInfo.Name = "btnToExcelManInfo";
			this.btnToExcelManInfo.Size = new System.Drawing.Size(145, 23);
			this.btnToExcelManInfo.TabIndex = 68;
			this.btnToExcelManInfo.Text = "Экспорт в Excel";
			this.btnToExcelManInfo.UseVisualStyleBackColor = true;
			this.btnToExcelManInfo.Click += new System.EventHandler(this.BtnToExcelManInfoClick);
			// 
			// btnGetBirthday
			// 
			this.btnGetBirthday.Location = new System.Drawing.Point(156, 94);
			this.btnGetBirthday.Name = "btnGetBirthday";
			this.btnGetBirthday.Size = new System.Drawing.Size(152, 23);
			this.btnGetBirthday.TabIndex = 77;
			this.btnGetBirthday.Text = "Отобразить";
			this.btnGetBirthday.UseVisualStyleBackColor = true;
			this.btnGetBirthday.Click += new System.EventHandler(this.BtnGetBirthdayClick);
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(170, 70);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(19, 17);
			this.label6.TabIndex = 42;
			this.label6.Text = "до";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(25, 70);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(20, 17);
			this.label7.TabIndex = 41;
			this.label7.Text = "От";
			// 
			// label8
			// 
			this.label8.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label8.Location = new System.Drawing.Point(8, 43);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(345, 16);
			this.label8.TabIndex = 40;
			this.label8.Text = "Найти дни рождения в заданном диапазоне:";
			// 
			// pnlInfo
			// 
			this.pnlInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.pnlInfo.Controls.Add(this.label1);
			this.pnlInfo.Controls.Add(this.lblAddManStatus);
			this.pnlInfo.Controls.Add(this.tbFName);
			this.pnlInfo.Controls.Add(this.tbName);
			this.pnlInfo.Controls.Add(this.tbOName);
			this.pnlInfo.Controls.Add(this.tbAddress);
			this.pnlInfo.Controls.Add(this.tbTel);
			this.pnlInfo.Controls.Add(this.btnAddMan);
			this.pnlInfo.Controls.Add(this.tbEmail);
			this.pnlInfo.Controls.Add(this.tbManComment);
			this.pnlInfo.Controls.Add(this.dtpBirthday);
			this.pnlInfo.Controls.Add(this.tbBirthday);
			this.pnlInfo.Location = new System.Drawing.Point(9, 419);
			this.pnlInfo.Name = "pnlInfo";
			this.pnlInfo.Size = new System.Drawing.Size(939, 91);
			this.pnlInfo.TabIndex = 39;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.Location = new System.Drawing.Point(3, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(186, 16);
			this.label1.TabIndex = 2;
			this.label1.Text = "Добавить строку в таблицу:";
			// 
			// lblAddManStatus
			// 
			this.lblAddManStatus.Location = new System.Drawing.Point(30, 45);
			this.lblAddManStatus.Name = "lblAddManStatus";
			this.lblAddManStatus.Size = new System.Drawing.Size(746, 23);
			this.lblAddManStatus.TabIndex = 38;
			// 
			// tbFName
			// 
			this.tbFName.ForeColor = System.Drawing.SystemColors.WindowFrame;
			this.tbFName.Location = new System.Drawing.Point(30, 19);
			this.tbFName.Name = "tbFName";
			this.tbFName.Size = new System.Drawing.Size(100, 20);
			this.tbFName.TabIndex = 3;
			this.tbFName.Tag = "Фамилия";
			this.tbFName.Text = "Фамилия";
			this.tbFName.Enter += new System.EventHandler(this.TbInputEnter);
			this.tbFName.KeyUp += new System.Windows.Forms.KeyEventHandler(this.AddManCtrlsKeyUp);
			this.tbFName.Leave += new System.EventHandler(this.TbInputLeave);
			// 
			// tbName
			// 
			this.tbName.ForeColor = System.Drawing.SystemColors.WindowFrame;
			this.tbName.Location = new System.Drawing.Point(136, 19);
			this.tbName.Name = "tbName";
			this.tbName.Size = new System.Drawing.Size(100, 20);
			this.tbName.TabIndex = 4;
			this.tbName.Tag = "Имя";
			this.tbName.Text = "Имя";
			this.tbName.Enter += new System.EventHandler(this.TbInputEnter);
			this.tbName.KeyUp += new System.Windows.Forms.KeyEventHandler(this.AddManCtrlsKeyUp);
			this.tbName.Leave += new System.EventHandler(this.TbInputLeave);
			// 
			// tbOName
			// 
			this.tbOName.ForeColor = System.Drawing.SystemColors.WindowFrame;
			this.tbOName.Location = new System.Drawing.Point(242, 19);
			this.tbOName.Name = "tbOName";
			this.tbOName.Size = new System.Drawing.Size(100, 20);
			this.tbOName.TabIndex = 5;
			this.tbOName.Tag = "Отчество";
			this.tbOName.Text = "Отчество";
			this.tbOName.Enter += new System.EventHandler(this.TbInputEnter);
			this.tbOName.KeyUp += new System.Windows.Forms.KeyEventHandler(this.AddManCtrlsKeyUp);
			this.tbOName.Leave += new System.EventHandler(this.TbInputLeave);
			// 
			// tbAddress
			// 
			this.tbAddress.ForeColor = System.Drawing.SystemColors.WindowFrame;
			this.tbAddress.Location = new System.Drawing.Point(348, 19);
			this.tbAddress.Name = "tbAddress";
			this.tbAddress.Size = new System.Drawing.Size(100, 20);
			this.tbAddress.TabIndex = 6;
			this.tbAddress.Tag = "Адрес";
			this.tbAddress.Text = "Адрес";
			this.tbAddress.Enter += new System.EventHandler(this.TbInputEnter);
			this.tbAddress.KeyUp += new System.Windows.Forms.KeyEventHandler(this.AddManCtrlsKeyUp);
			this.tbAddress.Leave += new System.EventHandler(this.TbInputLeave);
			// 
			// tbTel
			// 
			this.tbTel.ForeColor = System.Drawing.SystemColors.WindowFrame;
			this.tbTel.Location = new System.Drawing.Point(454, 19);
			this.tbTel.Name = "tbTel";
			this.tbTel.Size = new System.Drawing.Size(85, 20);
			this.tbTel.TabIndex = 7;
			this.tbTel.Tag = "Телефон";
			this.tbTel.Text = "Телефон";
			this.tbTel.Enter += new System.EventHandler(this.TbInputEnter);
			this.tbTel.KeyUp += new System.Windows.Forms.KeyEventHandler(this.AddManCtrlsKeyUp);
			this.tbTel.Leave += new System.EventHandler(this.TbInputLeave);
			// 
			// btnAddMan
			// 
			this.btnAddMan.Location = new System.Drawing.Point(782, 45);
			this.btnAddMan.Name = "btnAddMan";
			this.btnAddMan.Size = new System.Drawing.Size(152, 23);
			this.btnAddMan.TabIndex = 11;
			this.btnAddMan.Text = "Добавить";
			this.btnAddMan.UseVisualStyleBackColor = true;
			this.btnAddMan.Click += new System.EventHandler(this.BtnAddManClick);
			// 
			// tbEmail
			// 
			this.tbEmail.ForeColor = System.Drawing.SystemColors.WindowFrame;
			this.tbEmail.Location = new System.Drawing.Point(545, 19);
			this.tbEmail.Name = "tbEmail";
			this.tbEmail.Size = new System.Drawing.Size(105, 20);
			this.tbEmail.TabIndex = 8;
			this.tbEmail.Tag = "Электронная почта";
			this.tbEmail.Text = "Электронная почта";
			this.tbEmail.Enter += new System.EventHandler(this.TbInputEnter);
			this.tbEmail.KeyUp += new System.Windows.Forms.KeyEventHandler(this.AddManCtrlsKeyUp);
			this.tbEmail.Leave += new System.EventHandler(this.TbInputLeave);
			// 
			// tbManComment
			// 
			this.tbManComment.ForeColor = System.Drawing.SystemColors.WindowFrame;
			this.tbManComment.Location = new System.Drawing.Point(762, 19);
			this.tbManComment.Name = "tbManComment";
			this.tbManComment.Size = new System.Drawing.Size(172, 20);
			this.tbManComment.TabIndex = 10;
			this.tbManComment.Tag = "Комментарий";
			this.tbManComment.Text = "Комментарий";
			this.tbManComment.Enter += new System.EventHandler(this.TbInputEnter);
			this.tbManComment.KeyUp += new System.Windows.Forms.KeyEventHandler(this.AddManCtrlsKeyUp);
			this.tbManComment.Leave += new System.EventHandler(this.TbInputLeave);
			// 
			// dtpBirthday
			// 
			this.dtpBirthday.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpBirthday.Location = new System.Drawing.Point(656, 19);
			this.dtpBirthday.Name = "dtpBirthday";
			this.dtpBirthday.ShowCheckBox = true;
			this.dtpBirthday.Size = new System.Drawing.Size(100, 20);
			this.dtpBirthday.TabIndex = 33;
			// 
			// tbBirthday
			// 
			this.tbBirthday.ForeColor = System.Drawing.SystemColors.WindowFrame;
			this.tbBirthday.Location = new System.Drawing.Point(656, 19);
			this.tbBirthday.Name = "tbBirthday";
			this.tbBirthday.Size = new System.Drawing.Size(100, 20);
			this.tbBirthday.TabIndex = 9;
			this.tbBirthday.Tag = "Дата рождения";
			this.tbBirthday.Text = "Дата рождения";
			this.tbBirthday.Enter += new System.EventHandler(this.TbBirthdayEnter);
			this.tbBirthday.KeyUp += new System.Windows.Forms.KeyEventHandler(this.AddManCtrlsKeyUp);
			// 
			// btnGetAllManInfo
			// 
			this.btnGetAllManInfo.Location = new System.Drawing.Point(509, 6);
			this.btnGetAllManInfo.Name = "btnGetAllManInfo";
			this.btnGetAllManInfo.Size = new System.Drawing.Size(202, 23);
			this.btnGetAllManInfo.TabIndex = 37;
			this.btnGetAllManInfo.Text = "Отобразить информацию обо всех людях";
			this.btnGetAllManInfo.UseVisualStyleBackColor = true;
			this.btnGetAllManInfo.Visible = false;
			this.btnGetAllManInfo.Click += new System.EventHandler(this.BtnGetAllManInfoClick);
			// 
			// lblManInfoStatus
			// 
			this.lblManInfoStatus.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lblManInfoStatus.Location = new System.Drawing.Point(8, 10);
			this.lblManInfoStatus.Name = "lblManInfoStatus";
			this.lblManInfoStatus.Size = new System.Drawing.Size(756, 23);
			this.lblManInfoStatus.TabIndex = 36;
			this.lblManInfoStatus.Text = "Здесь отображается информация о людях";
			// 
			// dgvManInfo
			// 
			this.dgvManInfo.AllowUserToAddRows = false;
			this.dgvManInfo.AllowUserToDeleteRows = false;
			this.dgvManInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.dgvManInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvManInfo.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
			this.dgvManInfo.Location = new System.Drawing.Point(8, 124);
			this.dgvManInfo.Name = "dgvManInfo";
			this.dgvManInfo.Size = new System.Drawing.Size(940, 292);
			this.dgvManInfo.TabIndex = 34;
			this.dgvManInfo.Tag = "1";
			this.dgvManInfo.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvCellClick);
			// 
			// cbMonthFrom
			// 
			this.cbMonthFrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbMonthFrom.FormattingEnabled = true;
			this.cbMonthFrom.Location = new System.Drawing.Point(96, 67);
			this.cbMonthFrom.Name = "cbMonthFrom";
			this.cbMonthFrom.Size = new System.Drawing.Size(68, 21);
			this.cbMonthFrom.TabIndex = 74;
			// 
			// tabAchievements
			// 
			this.tabAchievements.Controls.Add(this.panel6);
			this.tabAchievements.Controls.Add(this.dgvAchievements);
			this.tabAchievements.Controls.Add(this.label27);
			this.tabAchievements.Location = new System.Drawing.Point(4, 22);
			this.tabAchievements.Name = "tabAchievements";
			this.tabAchievements.Size = new System.Drawing.Size(956, 518);
			this.tabAchievements.TabIndex = 5;
			this.tabAchievements.Text = "Значки";
			this.tabAchievements.UseVisualStyleBackColor = true;
			// 
			// panel6
			// 
			this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.panel6.Controls.Add(this.label28);
			this.panel6.Controls.Add(this.lblAddAchievementStatus);
			this.panel6.Controls.Add(this.tbAchievementComment);
			this.panel6.Controls.Add(this.cbAchievementName);
			this.panel6.Controls.Add(this.cbAchievementFIO);
			this.panel6.Controls.Add(this.btnAddAchievement);
			this.panel6.Controls.Add(this.dtpAchievement);
			this.panel6.Controls.Add(this.label30);
			this.panel6.Controls.Add(this.label31);
			this.panel6.Controls.Add(this.label33);
			this.panel6.Controls.Add(this.label35);
			this.panel6.Location = new System.Drawing.Point(8, 423);
			this.panel6.Name = "panel6";
			this.panel6.Size = new System.Drawing.Size(940, 92);
			this.panel6.TabIndex = 46;
			// 
			// label28
			// 
			this.label28.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label28.Location = new System.Drawing.Point(3, 0);
			this.label28.Name = "label28";
			this.label28.Size = new System.Drawing.Size(194, 16);
			this.label28.TabIndex = 14;
			this.label28.Text = "Добавить строку в таблицу:";
			// 
			// lblAddAchievementStatus
			// 
			this.lblAddAchievementStatus.Location = new System.Drawing.Point(30, 57);
			this.lblAddAchievementStatus.Name = "lblAddAchievementStatus";
			this.lblAddAchievementStatus.Size = new System.Drawing.Size(478, 23);
			this.lblAddAchievementStatus.TabIndex = 44;
			// 
			// tbAchievementComment
			// 
			this.tbAchievementComment.Location = new System.Drawing.Point(479, 36);
			this.tbAchievementComment.Name = "tbAchievementComment";
			this.tbAchievementComment.Size = new System.Drawing.Size(187, 20);
			this.tbAchievementComment.TabIndex = 18;
			// 
			// cbAchievementName
			// 
			this.cbAchievementName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
			this.cbAchievementName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
			this.cbAchievementName.FormattingEnabled = true;
			this.cbAchievementName.Location = new System.Drawing.Point(246, 36);
			this.cbAchievementName.Name = "cbAchievementName";
			this.cbAchievementName.Size = new System.Drawing.Size(121, 21);
			this.cbAchievementName.TabIndex = 16;
			// 
			// cbAchievementFIO
			// 
			this.cbAchievementFIO.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
			this.cbAchievementFIO.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
			this.cbAchievementFIO.FormattingEnabled = true;
			this.cbAchievementFIO.Location = new System.Drawing.Point(30, 36);
			this.cbAchievementFIO.Name = "cbAchievementFIO";
			this.cbAchievementFIO.Size = new System.Drawing.Size(210, 21);
			this.cbAchievementFIO.TabIndex = 15;
			// 
			// btnAddAchievement
			// 
			this.btnAddAchievement.Location = new System.Drawing.Point(514, 62);
			this.btnAddAchievement.Name = "btnAddAchievement";
			this.btnAddAchievement.Size = new System.Drawing.Size(152, 23);
			this.btnAddAchievement.TabIndex = 19;
			this.btnAddAchievement.Text = "Добавить";
			this.btnAddAchievement.UseVisualStyleBackColor = true;
			// 
			// dtpAchievement
			// 
			this.dtpAchievement.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpAchievement.Location = new System.Drawing.Point(373, 36);
			this.dtpAchievement.Name = "dtpAchievement";
			this.dtpAchievement.ShowCheckBox = true;
			this.dtpAchievement.Size = new System.Drawing.Size(100, 20);
			this.dtpAchievement.TabIndex = 17;
			// 
			// label30
			// 
			this.label30.Location = new System.Drawing.Point(479, 21);
			this.label30.Name = "label30";
			this.label30.Size = new System.Drawing.Size(84, 19);
			this.label30.TabIndex = 43;
			this.label30.Text = "Комментарий:";
			// 
			// label31
			// 
			this.label31.Location = new System.Drawing.Point(246, 19);
			this.label31.Name = "label31";
			this.label31.Size = new System.Drawing.Size(100, 19);
			this.label31.TabIndex = 40;
			this.label31.Text = "Значок:";
			// 
			// label33
			// 
			this.label33.Location = new System.Drawing.Point(30, 19);
			this.label33.Name = "label33";
			this.label33.Size = new System.Drawing.Size(100, 19);
			this.label33.TabIndex = 20;
			this.label33.Text = "ФИО:";
			// 
			// label35
			// 
			this.label35.Location = new System.Drawing.Point(373, 19);
			this.label35.Name = "label35";
			this.label35.Size = new System.Drawing.Size(100, 16);
			this.label35.TabIndex = 22;
			this.label35.Text = "Дата вручения:";
			// 
			// dgvAchievements
			// 
			this.dgvAchievements.AllowUserToAddRows = false;
			this.dgvAchievements.AllowUserToDeleteRows = false;
			this.dgvAchievements.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.dgvAchievements.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvAchievements.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
			this.dgvAchievements.Location = new System.Drawing.Point(8, 34);
			this.dgvAchievements.Name = "dgvAchievements";
			this.dgvAchievements.Size = new System.Drawing.Size(940, 383);
			this.dgvAchievements.TabIndex = 40;
			this.dgvAchievements.Tag = "4";
			// 
			// label27
			// 
			this.label27.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label27.Location = new System.Drawing.Point(8, 8);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(704, 23);
			this.label27.TabIndex = 39;
			this.label27.Text = "Здесь можно задать значки, выданные человеку";
			// 
			// tabPlaces
			// 
			this.tabPlaces.Controls.Add(this.btnToExcelPlaces);
			this.tabPlaces.Controls.Add(this.pnlPlaces);
			this.tabPlaces.Controls.Add(this.label4);
			this.tabPlaces.Controls.Add(this.dgvPlaces);
			this.tabPlaces.Location = new System.Drawing.Point(4, 22);
			this.tabPlaces.Name = "tabPlaces";
			this.tabPlaces.Size = new System.Drawing.Size(956, 518);
			this.tabPlaces.TabIndex = 2;
			this.tabPlaces.Text = "Места принятия обетов";
			this.tabPlaces.UseVisualStyleBackColor = true;
			// 
			// btnToExcelPlaces
			// 
			this.btnToExcelPlaces.Location = new System.Drawing.Point(803, 10);
			this.btnToExcelPlaces.Name = "btnToExcelPlaces";
			this.btnToExcelPlaces.Size = new System.Drawing.Size(145, 23);
			this.btnToExcelPlaces.TabIndex = 68;
			this.btnToExcelPlaces.Text = "Экспорт в Excel";
			this.btnToExcelPlaces.UseVisualStyleBackColor = true;
			this.btnToExcelPlaces.Click += new System.EventHandler(this.BtnToExcelPlacesClick);
			// 
			// pnlPlaces
			// 
			this.pnlPlaces.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.pnlPlaces.Controls.Add(this.tbPlace);
			this.pnlPlaces.Controls.Add(this.label9);
			this.pnlPlaces.Controls.Add(this.lblAddPlaceStatus);
			this.pnlPlaces.Controls.Add(this.tbPlaceComment);
			this.pnlPlaces.Controls.Add(this.btnAddPlace);
			this.pnlPlaces.Location = new System.Drawing.Point(8, 428);
			this.pnlPlaces.Name = "pnlPlaces";
			this.pnlPlaces.Size = new System.Drawing.Size(940, 82);
			this.pnlPlaces.TabIndex = 40;
			// 
			// tbPlace
			// 
			this.tbPlace.ForeColor = System.Drawing.SystemColors.WindowFrame;
			this.tbPlace.Location = new System.Drawing.Point(27, 19);
			this.tbPlace.Name = "tbPlace";
			this.tbPlace.Size = new System.Drawing.Size(215, 20);
			this.tbPlace.TabIndex = 14;
			this.tbPlace.Tag = "Место, где принимают обет";
			this.tbPlace.Text = "Место, где принимают обет";
			// 
			// label9
			// 
			this.label9.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label9.Location = new System.Drawing.Point(0, 0);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(189, 16);
			this.label9.TabIndex = 13;
			this.label9.Text = "Добавить строку в таблицу:";
			// 
			// lblAddPlaceStatus
			// 
			this.lblAddPlaceStatus.Location = new System.Drawing.Point(25, 47);
			this.lblAddPlaceStatus.Name = "lblAddPlaceStatus";
			this.lblAddPlaceStatus.Size = new System.Drawing.Size(309, 23);
			this.lblAddPlaceStatus.TabIndex = 39;
			// 
			// tbPlaceComment
			// 
			this.tbPlaceComment.ForeColor = System.Drawing.SystemColors.WindowFrame;
			this.tbPlaceComment.Location = new System.Drawing.Point(248, 19);
			this.tbPlaceComment.Name = "tbPlaceComment";
			this.tbPlaceComment.Size = new System.Drawing.Size(215, 20);
			this.tbPlaceComment.TabIndex = 15;
			this.tbPlaceComment.Tag = "Комментарий";
			this.tbPlaceComment.Text = "Комментарий";
			this.tbPlaceComment.Enter += new System.EventHandler(this.TbInputEnter);
			this.tbPlaceComment.KeyUp += new System.Windows.Forms.KeyEventHandler(this.AddPlaceCtrlsKeyUp);
			this.tbPlaceComment.Leave += new System.EventHandler(this.TbInputLeave);
			// 
			// btnAddPlace
			// 
			this.btnAddPlace.Location = new System.Drawing.Point(336, 45);
			this.btnAddPlace.Name = "btnAddPlace";
			this.btnAddPlace.Size = new System.Drawing.Size(127, 23);
			this.btnAddPlace.TabIndex = 16;
			this.btnAddPlace.Text = "Добавить";
			this.btnAddPlace.UseVisualStyleBackColor = true;
			this.btnAddPlace.Click += new System.EventHandler(this.BtnAddPlaceClick);
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label4.Location = new System.Drawing.Point(8, 8);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(704, 23);
			this.label4.TabIndex = 38;
			this.label4.Text = "Здесь отображаются названия мест, где люди принимали обеты";
			// 
			// dgvPlaces
			// 
			this.dgvPlaces.AllowUserToAddRows = false;
			this.dgvPlaces.AllowUserToDeleteRows = false;
			this.dgvPlaces.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.dgvPlaces.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvPlaces.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
			this.dgvPlaces.Location = new System.Drawing.Point(8, 34);
			this.dgvPlaces.Name = "dgvPlaces";
			this.dgvPlaces.Size = new System.Drawing.Size(940, 383);
			this.dgvPlaces.TabIndex = 17;
			this.dgvPlaces.Tag = "2";
			this.dgvPlaces.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvCellClick);
			// 
			// tabSettings
			// 
			this.tabSettings.Controls.Add(this.panel5);
			this.tabSettings.Controls.Add(this.label22);
			this.tabSettings.Controls.Add(this.panel4);
			this.tabSettings.Controls.Add(this.panel3);
			this.tabSettings.Controls.Add(this.lblSettingsInfo);
			this.tabSettings.Controls.Add(this.panel2);
			this.tabSettings.Location = new System.Drawing.Point(4, 22);
			this.tabSettings.Name = "tabSettings";
			this.tabSettings.Padding = new System.Windows.Forms.Padding(3);
			this.tabSettings.Size = new System.Drawing.Size(956, 518);
			this.tabSettings.TabIndex = 4;
			this.tabSettings.Text = "Дополнительно";
			this.tabSettings.UseVisualStyleBackColor = true;
			// 
			// panel5
			// 
			this.panel5.Controls.Add(this.dgvAchieveNames);
			this.panel5.Controls.Add(this.label26);
			this.panel5.Location = new System.Drawing.Point(11, 354);
			this.panel5.Name = "panel5";
			this.panel5.Size = new System.Drawing.Size(425, 136);
			this.panel5.TabIndex = 48;
			// 
			// dgvAchieveNames
			// 
			this.dgvAchieveNames.AllowUserToOrderColumns = true;
			this.dgvAchieveNames.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.dgvAchieveNames.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvAchieveNames.Location = new System.Drawing.Point(5, 19);
			this.dgvAchieveNames.Name = "dgvAchieveNames";
			this.dgvAchieveNames.Size = new System.Drawing.Size(414, 111);
			this.dgvAchieveNames.TabIndex = 2;
			// 
			// label26
			// 
			this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label26.Location = new System.Drawing.Point(3, 0);
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size(164, 16);
			this.label26.TabIndex = 1;
			this.label26.Text = "Имена и описания значков:";
			// 
			// label22
			// 
			this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label22.Location = new System.Drawing.Point(579, 495);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(374, 18);
			this.label22.TabIndex = 47;
			this.label22.Text = "Автор: Алексей Степанов. По всем вопросам: Arsennikum@gmail.com";
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.lblSyncStatus);
			this.panel4.Controls.Add(this.btnSyncSave);
			this.panel4.Controls.Add(this.btnSyncPathBrowse);
			this.panel4.Controls.Add(this.btnSyncOpenBrowse);
			this.panel4.Controls.Add(this.lblSyncFileCountStatus);
			this.panel4.Controls.Add(this.tbSyncPath);
			this.panel4.Controls.Add(this.label25);
			this.panel4.Controls.Add(this.label24);
			this.panel4.Controls.Add(this.label23);
			this.panel4.Location = new System.Drawing.Point(11, 238);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(714, 109);
			this.panel4.TabIndex = 46;
			// 
			// lblSyncStatus
			// 
			this.lblSyncStatus.Location = new System.Drawing.Point(5, 78);
			this.lblSyncStatus.Name = "lblSyncStatus";
			this.lblSyncStatus.Size = new System.Drawing.Size(359, 23);
			this.lblSyncStatus.TabIndex = 8;
			this.lblSyncStatus.Text = "lblSyncStatus";
			// 
			// btnSyncSave
			// 
			this.btnSyncSave.Enabled = false;
			this.btnSyncSave.Location = new System.Drawing.Point(370, 78);
			this.btnSyncSave.Name = "btnSyncSave";
			this.btnSyncSave.Size = new System.Drawing.Size(151, 23);
			this.btnSyncSave.TabIndex = 9;
			this.btnSyncSave.Text = "Сохранить";
			this.btnSyncSave.UseVisualStyleBackColor = true;
			this.btnSyncSave.Click += new System.EventHandler(this.BtnSyncSaveClick);
			// 
			// btnSyncPathBrowse
			// 
			this.btnSyncPathBrowse.Enabled = false;
			this.btnSyncPathBrowse.Location = new System.Drawing.Point(417, 52);
			this.btnSyncPathBrowse.Name = "btnSyncPathBrowse";
			this.btnSyncPathBrowse.Size = new System.Drawing.Size(75, 21);
			this.btnSyncPathBrowse.TabIndex = 8;
			this.btnSyncPathBrowse.Text = "Обзор...";
			this.btnSyncPathBrowse.UseVisualStyleBackColor = true;
			this.btnSyncPathBrowse.Click += new System.EventHandler(this.BtnSyncPathBrowseClick);
			// 
			// btnSyncOpenBrowse
			// 
			this.btnSyncOpenBrowse.Location = new System.Drawing.Point(417, 26);
			this.btnSyncOpenBrowse.Name = "btnSyncOpenBrowse";
			this.btnSyncOpenBrowse.Size = new System.Drawing.Size(75, 21);
			this.btnSyncOpenBrowse.TabIndex = 6;
			this.btnSyncOpenBrowse.Text = "Обзор...";
			this.btnSyncOpenBrowse.UseVisualStyleBackColor = true;
			this.btnSyncOpenBrowse.Click += new System.EventHandler(this.BtnSyncOpenBrowseClick);
			// 
			// lblSyncFileCountStatus
			// 
			this.lblSyncFileCountStatus.Location = new System.Drawing.Point(498, 28);
			this.lblSyncFileCountStatus.Name = "lblSyncFileCountStatus";
			this.lblSyncFileCountStatus.Size = new System.Drawing.Size(202, 19);
			this.lblSyncFileCountStatus.TabIndex = 5;
			this.lblSyncFileCountStatus.Text = "Выбрано 0 файлов";
			// 
			// tbSyncPath
			// 
			this.tbSyncPath.Location = new System.Drawing.Point(205, 52);
			this.tbSyncPath.Name = "tbSyncPath";
			this.tbSyncPath.Size = new System.Drawing.Size(206, 20);
			this.tbSyncPath.TabIndex = 7;
			// 
			// label25
			// 
			this.label25.Location = new System.Drawing.Point(3, 54);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(196, 18);
			this.label25.TabIndex = 3;
			this.label25.Text = "Куда сохранить объединённую базу:";
			// 
			// label24
			// 
			this.label24.Location = new System.Drawing.Point(3, 29);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(408, 18);
			this.label24.TabIndex = 1;
			this.label24.Text = "Выберите несколько файлов (для этого зажмите ctrl или выделите курсором):";
			// 
			// label23
			// 
			this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label23.Location = new System.Drawing.Point(3, 7);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(273, 18);
			this.label23.TabIndex = 0;
			this.label23.Text = "Объединить несколько файлов баз данных в одну:";
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.btnBackupBrowse);
			this.panel3.Controls.Add(this.lblBackupStatus);
			this.panel3.Controls.Add(this.btnBackup);
			this.panel3.Controls.Add(this.tbBackupPath);
			this.panel3.Controls.Add(this.label21);
			this.panel3.Controls.Add(this.label3);
			this.panel3.Location = new System.Drawing.Point(11, 129);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(715, 103);
			this.panel3.TabIndex = 45;
			// 
			// btnBackupBrowse
			// 
			this.btnBackupBrowse.Location = new System.Drawing.Point(626, 35);
			this.btnBackupBrowse.Name = "btnBackupBrowse";
			this.btnBackupBrowse.Size = new System.Drawing.Size(75, 21);
			this.btnBackupBrowse.TabIndex = 43;
			this.btnBackupBrowse.Text = "Обзор...";
			this.btnBackupBrowse.UseVisualStyleBackColor = true;
			this.btnBackupBrowse.Click += new System.EventHandler(this.BtnBackupBrowseClick);
			// 
			// lblBackupStatus
			// 
			this.lblBackupStatus.Location = new System.Drawing.Point(5, 61);
			this.lblBackupStatus.Name = "lblBackupStatus";
			this.lblBackupStatus.Size = new System.Drawing.Size(486, 23);
			this.lblBackupStatus.TabIndex = 44;
			this.lblBackupStatus.Text = "lblBackupStatus";
			// 
			// btnBackup
			// 
			this.btnBackup.Location = new System.Drawing.Point(497, 61);
			this.btnBackup.Name = "btnBackup";
			this.btnBackup.Size = new System.Drawing.Size(203, 23);
			this.btnBackup.TabIndex = 44;
			this.btnBackup.Text = "Сохранить копию базы данных";
			this.btnBackup.UseVisualStyleBackColor = true;
			this.btnBackup.Click += new System.EventHandler(this.BtnBackupClick);
			// 
			// tbBackupPath
			// 
			this.tbBackupPath.Location = new System.Drawing.Point(102, 35);
			this.tbBackupPath.Name = "tbBackupPath";
			this.tbBackupPath.Size = new System.Drawing.Size(523, 20);
			this.tbBackupPath.TabIndex = 42;
			this.tbBackupPath.Text = "Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @\"\\БД для ОТ\\\"" +
			"";
			// 
			// label21
			// 
			this.label21.Location = new System.Drawing.Point(5, 37);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(91, 18);
			this.label21.TabIndex = 41;
			this.label21.Text = "Куда сохранить:";
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label3.Location = new System.Drawing.Point(1, 13);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(234, 23);
			this.label3.TabIndex = 40;
			this.label3.Text = "Выполнить резервное копирование:";
			// 
			// lblSettingsInfo
			// 
			this.lblSettingsInfo.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lblSettingsInfo.Location = new System.Drawing.Point(8, 12);
			this.lblSettingsInfo.Name = "lblSettingsInfo";
			this.lblSettingsInfo.Size = new System.Drawing.Size(704, 23);
			this.lblSettingsInfo.TabIndex = 39;
			this.lblSettingsInfo.Text = "Здесь выполняются дополнительные задачи";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.label15);
			this.panel2.Controls.Add(this.btnSaveSettings);
			this.panel2.Controls.Add(this.tbDBPath);
			this.panel2.Controls.Add(this.btnDBPathBrowse);
			this.panel2.Controls.Add(this.lblSaveSettingsStatus);
			this.panel2.Location = new System.Drawing.Point(11, 38);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(715, 85);
			this.panel2.TabIndex = 3;
			// 
			// label15
			// 
			this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label15.Location = new System.Drawing.Point(3, 10);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(164, 16);
			this.label15.TabIndex = 0;
			this.label15.Text = "Путь до файла базы данных:";
			// 
			// btnSaveSettings
			// 
			this.btnSaveSettings.Location = new System.Drawing.Point(626, 31);
			this.btnSaveSettings.Name = "btnSaveSettings";
			this.btnSaveSettings.Size = new System.Drawing.Size(75, 23);
			this.btnSaveSettings.TabIndex = 3;
			this.btnSaveSettings.Text = "Сохранить";
			this.btnSaveSettings.UseVisualStyleBackColor = true;
			this.btnSaveSettings.Click += new System.EventHandler(this.BtnSaveSettingsClick);
			// 
			// tbDBPath
			// 
			this.tbDBPath.Location = new System.Drawing.Point(5, 33);
			this.tbDBPath.Name = "tbDBPath";
			this.tbDBPath.Size = new System.Drawing.Size(538, 20);
			this.tbDBPath.TabIndex = 1;
			this.tbDBPath.Text = "C:\\db\\SocietyOfSobriety.accdb";
			// 
			// btnDBPathBrowse
			// 
			this.btnDBPathBrowse.Location = new System.Drawing.Point(543, 33);
			this.btnDBPathBrowse.Name = "btnDBPathBrowse";
			this.btnDBPathBrowse.Size = new System.Drawing.Size(75, 21);
			this.btnDBPathBrowse.TabIndex = 2;
			this.btnDBPathBrowse.Text = "Обзор...";
			this.btnDBPathBrowse.UseVisualStyleBackColor = true;
			this.btnDBPathBrowse.Click += new System.EventHandler(this.BtnDBPathBrowseClick);
			// 
			// lblSaveSettingsStatus
			// 
			this.lblSaveSettingsStatus.Location = new System.Drawing.Point(3, 59);
			this.lblSaveSettingsStatus.Name = "lblSaveSettingsStatus";
			this.lblSaveSettingsStatus.Size = new System.Drawing.Size(615, 20);
			this.lblSaveSettingsStatus.TabIndex = 3;
			this.lblSaveSettingsStatus.Text = "lblSaveSettingsStatus";
			// 
			// textBox2
			// 
			this.textBox2.ForeColor = System.Drawing.SystemColors.WindowFrame;
			this.textBox2.Location = new System.Drawing.Point(176, 22);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(123, 20);
			this.textBox2.TabIndex = 16;
			this.textBox2.Tag = "Комментарий";
			this.textBox2.Text = "Комментарий";
			// 
			// textBox1
			// 
			this.textBox1.ForeColor = System.Drawing.SystemColors.WindowFrame;
			this.textBox1.Location = new System.Drawing.Point(31, 320);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(143, 20);
			this.textBox1.TabIndex = 15;
			this.textBox1.Tag = "Место, где принят обет";
			this.textBox1.Text = "Место, где принят обет";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(4, 3);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(151, 16);
			this.label2.TabIndex = 14;
			this.label2.Text = "Добавить строку в таблицу:";
			// 
			// panel1
			// 
			this.panel1.Location = new System.Drawing.Point(6, 302);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(309, 74);
			this.panel1.TabIndex = 14;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(964, 544);
			this.Controls.Add(this.tabControl1);
			this.Name = "MainForm";
			this.Text = "База данных для обществ трезвения";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFormFormClosing);
			this.tabControl1.ResumeLayout(false);
			this.tabIntervals.ResumeLayout(false);
			this.pnlIntervals.ResumeLayout(false);
			this.pnlIntervals.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvIntervals)).EndInit();
			this.tabInfo.ResumeLayout(false);
			this.pnlInfo.ResumeLayout(false);
			this.pnlInfo.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvManInfo)).EndInit();
			this.tabAchievements.ResumeLayout(false);
			this.panel6.ResumeLayout(false);
			this.panel6.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvAchievements)).EndInit();
			this.tabPlaces.ResumeLayout(false);
			this.pnlPlaces.ResumeLayout(false);
			this.pnlPlaces.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvPlaces)).EndInit();
			this.tabSettings.ResumeLayout(false);
			this.panel5.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvAchieveNames)).EndInit();
			this.panel4.ResumeLayout(false);
			this.panel4.PerformLayout();
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.TextBox tbPlace;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.DataGridView dgvAchievements;
		private System.Windows.Forms.Label label35;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.DateTimePicker dtpAchievement;
		private System.Windows.Forms.Button btnAddAchievement;
		private System.Windows.Forms.ComboBox cbAchievementFIO;
		private System.Windows.Forms.ComboBox cbAchievementName;
		private System.Windows.Forms.TextBox tbAchievementComment;
		private System.Windows.Forms.Label lblAddAchievementStatus;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Panel panel6;
		private System.Windows.Forms.DataGridView dgvAchieveNames;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.TabPage tabAchievements;
		private System.Windows.Forms.Label label22;

		private System.Windows.Forms.Button btnDBPathBrowse;
		private System.Windows.Forms.Button btnBackupBrowse;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.TextBox tbSyncPath;
		private System.Windows.Forms.Label lblSyncFileCountStatus;
		private System.Windows.Forms.Button btnSyncOpenBrowse;
		private System.Windows.Forms.Button btnSyncSave;
		private System.Windows.Forms.Label lblSyncStatus;
		private System.Windows.Forms.Button btnSyncPathBrowse;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Label lblSaveSettingsStatus;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.ComboBox cbDayFrom;
		private System.Windows.Forms.ComboBox cbMonthFrom;
		private System.Windows.Forms.ComboBox cbDayTo;
		private System.Windows.Forms.ComboBox cbMonthTo;
		private System.Windows.Forms.Button btnToExcelPlaces;
		private System.Windows.Forms.Button btnToExcelManInfo;
		private System.Windows.Forms.Button btnToExcelIntervals;
		private System.Windows.Forms.Label lblBackupStatus;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.TextBox tbBackupPath;
		private System.Windows.Forms.Button btnBackup;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label lblSettingsInfo;
		private System.Windows.Forms.Button btnGetManVows;
		private System.Windows.Forms.Button btnFindManInfo;
		private System.Windows.Forms.Button btnFindPlace;
		private System.Windows.Forms.Button btnGetAllIntervals;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button btnSaveSettings;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox tbDBPath;
		private System.Windows.Forms.TabPage tabSettings;
		private System.Windows.Forms.Panel pnlIntervals;
		private System.Windows.Forms.Panel pnlPlaces;
		private System.Windows.Forms.Panel pnlInfo;
		private System.Windows.Forms.Label lblAddIntervalStatus;
		private System.Windows.Forms.TextBox tbIntervalsComment;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.CheckBox cBoxIntervalsIsTrezv;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox cbIntervalPlace;
		private System.Windows.Forms.Label lblAddPlaceStatus;
		private System.Windows.Forms.Label lblAddManStatus;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label lblIntervalsStatus;
		private System.Windows.Forms.Label lblManInfoStatus;
		private System.Windows.Forms.Button btnGetAllManInfo;
		private System.Windows.Forms.DateTimePicker dtpNearVowFrom;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.DateTimePicker dtpNearVowTo;
		private System.Windows.Forms.Button btnGetNearVow;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.DateTimePicker dtpInVow;
		private System.Windows.Forms.Button btnGetInVow;
		private System.Windows.Forms.DataGridView dgvPlaces;
		private System.Windows.Forms.DataGridView dgvIntervals;
		private System.Windows.Forms.DataGridView dgvManInfo;
		private System.Windows.Forms.DateTimePicker dtpBirthday;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Button btnAddInterval;
		private System.Windows.Forms.ComboBox cbIntervalFIO;
		private System.Windows.Forms.DateTimePicker dtpDateBegin;
		private System.Windows.Forms.DateTimePicker dtpDateEnd;
		private System.Windows.Forms.ComboBox cbIntervalType;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TabPage tabIntervals;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox tbPlaceComment;
		private System.Windows.Forms.Button btnAddPlace;
		private System.Windows.Forms.Button btnGetBirthday;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		//private System.Windows.Forms.Button btnGetBirthday;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.TextBox textBox2;
		// del private System.Windows.Forms.Button btnAddPlace;
		private System.Windows.Forms.Button btnAddMan;
		private System.Windows.Forms.TextBox tbFName;
		private System.Windows.Forms.TextBox tbName;
		private System.Windows.Forms.TextBox tbOName;
		private System.Windows.Forms.TextBox tbAddress;
		private System.Windows.Forms.TextBox tbTel;
		private System.Windows.Forms.TextBox tbEmail;
		private System.Windows.Forms.TextBox tbBirthday;
		private System.Windows.Forms.TextBox tbManComment;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TabPage tabPlaces;
		private System.Windows.Forms.TabPage tabInfo;
		private System.Windows.Forms.TabControl tabControl1;
	}
}
