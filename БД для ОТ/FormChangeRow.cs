﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Алексей
 * Дата: 06.06.2014
 * Время: 16:40
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace БД_для_ОТ
{
	/// <summary>
	/// Description of FormChangeRow.
	/// </summary>
	public partial class FormChangeRow : Form
	{
		DataGridViewRow row;
		bool saved = false;
		int tI;
		
		public FormChangeRow(int typeI, DataGridViewRow rowInput)
		{
			InitializeComponent();
			
			row = rowInput;
			tI = typeI;
			switch (typeI)
			{
				case 1: // ManInfo
					this.Size = new Size(pnlMInfo.Size.Width + 24, pnlMInfo.Size.Height + 45);
					tbMInfoFName.Text = row.Cells["Фамилия"].Value.ToString();
					tbMInfoName.Text = row.Cells["Имя"].Value.ToString();
					tbMInfoOName.Text = row.Cells["Отчество"].Value.ToString();
					dtpMInfoBirthday.Value = (DateTime)row.Cells["Дата рождения"].Value;
					tbMInfoAddress.Text = row.Cells["Адрес"].Value.ToString();
					tbMInfoTel.Text = row.Cells["Телефон"].Value.ToString();
					tbMInfoEmail.Text = row.Cells["Электр.почта"].Value.ToString();
					tbMInfoComment.Text = row.Cells["Комментарий"].Value.ToString();
					break;
				case 2: // Places
					pnlMInfo.Visible = false;
					pnlPlaces.Location = pnlMInfo.Location;
					this.Size = new Size(pnlPlaces.Size.Width + 24, pnlPlaces.Size.Height + 45);
					tbPlace.Text = row.Cells["Место"].Value.ToString();
					tbPlaceComment.Text = row.Cells["Комментарий"].Value.ToString();
					break;
				case 3: // Intervals
					pnlMInfo.Visible = false;
					pnlIntervals.Location = pnlMInfo.Location;
					this.Size = new Size(pnlIntervals.Size.Width + 24, pnlIntervals.Size.Height + 45);
					cbIntervalFIO.DataSource = (row.DataGridView.DataSource as DataView).Table.DataSet.Tables["ManInfo"];
					cbIntervalFIO.DisplayMember = "FIO";
					cbIntervalFIO.ValueMember = "id";
					cbIntervalFIO.SelectedValue = row.Cells["idMan"].Value;
					cbIntervalPlace.DataSource = (row.DataGridView.DataSource as DataView).Table.DataSet.Tables["Places"];
					cbIntervalPlace.DisplayMember = "Место";
					cbIntervalPlace.ValueMember = "id";
					cbIntervalPlace.SelectedValue = row.Cells["idPlace"].Value;
					dtpDateBegin.Value = (DateTime)row.Cells["Начало"].Value;
					dtpDateEnd.Value = (DateTime)row.Cells["Конец"].Value;
					cbIntervalType.SelectedIndex = Convert.ToInt32(row.Cells["Обет?"].Value) == 0 ? 1 : 0; // Вроде тут наоборот, тогда и в бтнКлике поменяй
					cBoxIntervalsIsTrezv.Checked = (bool)row.Cells["Проведён трезво"].Value;
					tbIntervalsComment.Text = row.Cells["Комментарий"].Value.ToString();
					break;
			}
		}
		
		void BtnManClick(object sender, EventArgs e)
		{
			row.Cells["Фамилия"].Value = tbMInfoFName.Text;
			row.Cells["Имя"].Value = tbMInfoName.Text;
			row.Cells["Отчество"].Value = tbMInfoOName.Text;
			row.Cells["Дата рождения"].Value = dtpMInfoBirthday.Value;
			row.Cells["Адрес"].Value = tbMInfoAddress.Text;
			row.Cells["Телефон"].Value = tbMInfoTel.Text;
			row.Cells["Электр.почта"].Value = tbMInfoEmail.Text;
			row.Cells["Комментарий"].Value = tbMInfoComment.Text;
			saved = true;
			this.Close();
		}
		
		void BtnPlaceClick(object sender, EventArgs e)
		{
			row.Cells["Место"].Value = tbPlace.Text;
			row.Cells["Комментарий"].Value = tbPlaceComment.Text;
			saved = true;
			this.Close();
		}
		
		void BtnIntervalClick(object sender, EventArgs e)
		{
			if (cbIntervalFIO.SelectedValue == null || cbIntervalPlace.SelectedValue == null)
			{// Этого по идее быть не может
				MessageBox.Show("Вы задали не все значения. Изменение не произведено");
				return;
			}
			row.Cells["idMan"].Value = cbIntervalFIO.SelectedValue;
			row.Cells["idPlace"].Value = cbIntervalPlace.SelectedValue;
			row.Cells["Начало"].Value = dtpDateBegin.Value;
			row.Cells["Конец"].Value = dtpDateEnd.Value;
			row.Cells["Обет?"].Value = cbIntervalType.SelectedIndex == 0 ? 1 : 0;
			row.Cells["Проведён трезво"].Value = cBoxIntervalsIsTrezv.Checked;
			row.Cells["Комментарий"].Value = tbIntervalsComment.Text;
			saved = true;
			this.Close();
		}
		
		void FormChangeRowFormClosing(object sender, FormClosingEventArgs e)
		{
			if (!saved)
			{
				switch(tI)
				{
					case 1:
						if (!(
							(row.Cells["Фамилия"].Value as String == tbMInfoFName.Text)&&
							(row.Cells["Имя"].Value as String == tbMInfoName.Text)&&
							(row.Cells["Отчество"].Value as String == tbMInfoOName.Text)&&
							((DateTime)row.Cells["Дата рождения"].Value == dtpMInfoBirthday.Value)&&
							(row.Cells["Адрес"].Value as String == tbMInfoAddress.Text)&&
							(row.Cells["Телефон"].Value as String == tbMInfoTel.Text)&&
							(row.Cells["Электр.почта"].Value as String == tbMInfoEmail.Text)&&
							(row.Cells["Комментарий"].Value as String == tbMInfoComment.Text)
						))
						{
							DialogResult dres = MessageBox.Show("Изменения не сохранены. Сохранить?", "Закрытие окна",
							                                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning,
							                                    MessageBoxDefaultButton.Button1);
							switch(dres)
							{
								case DialogResult.Yes:
									BtnManClick(null, null);
									break;
								case DialogResult.Cancel:
									e.Cancel = true;
									break;
							}
						}
						break;
					case 2:
						if (!(
							(row.Cells["Место"].Value as String == tbPlace.Text)&&
							(row.Cells["Комментарий"].Value as String == tbPlaceComment.Text)
						))
						{
							DialogResult dres = MessageBox.Show("Изменения не сохранены. Сохранить?", "Закрытие окна",
							                                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning,
							                                    MessageBoxDefaultButton.Button1);
							switch(dres)
							{
								case DialogResult.Yes:
									BtnPlaceClick(null, null);
									break;
								case DialogResult.Cancel:
									e.Cancel = true;
									break;
							}
						}
						break;
					case 3:
						if (!(
							(row.Cells["idMan"].Value.Equals(cbIntervalFIO.SelectedValue))&&
							(row.Cells["idPlace"].Value.Equals(cbIntervalPlace.SelectedValue))&&
							((DateTime)row.Cells["Начало"].Value == dtpDateBegin.Value)&&
							((DateTime)row.Cells["Конец"].Value == dtpDateEnd.Value)&&
							(row.Cells["Обет?"].Value as bool? == (cbIntervalType.SelectedIndex == 0 ? true : false))&&
							(row.Cells["Проведён трезво"].Value as bool? == cBoxIntervalsIsTrezv.Checked)&&
							(row.Cells["Комментарий"].Value as String == tbIntervalsComment.Text)
						))
						{
							DialogResult dres = MessageBox.Show("Изменения не сохранены. Сохранить?", "Закрытие окна",
							                                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning,
							                                    MessageBoxDefaultButton.Button1);
							switch(dres)
							{
								case DialogResult.Yes:
									BtnIntervalClick(null, null);
									break;
								case DialogResult.Cancel:
									e.Cancel = true;
									break;
							}
						}
						break;
				}
			}
		}
		
		void BtnCancelClick(object sender, EventArgs e)
		{
			saved = true;
			this.Close();
		}
	}
}
