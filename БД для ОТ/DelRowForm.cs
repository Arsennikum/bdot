﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Алексей
 * Дата: 23.07.2014
 * Время: 19:57
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace БД_для_ОТ
{
	/// <summary>
	/// Description of DelRowForm.
	/// </summary>
	public partial class DelRowForm : Form
	{
		public DelRowForm(DataView dv, short tableType)
		{
			InitializeComponent();
			
			dataGridView1.DataSource = dv;
			dataGridView1.Columns["id"].Visible = false;
			dataGridView1.Columns["idMan"].Visible = false;
			dataGridView1.Columns["idPlace"].Visible = false;
			dataGridView1.Columns["Обет?"].Visible = false;
			dataGridView1.Columns["ФИО"].DisplayIndex = 0;
			dataGridView1.Columns["Обет/Промежуток"].DisplayIndex = 1;
			dataGridView1.Columns["Место принятия"].DisplayIndex = 3;
			dataGridView1.Columns["Начало"].DisplayIndex = 4;
			dataGridView1.Columns["Конец"].DisplayIndex = 5;
			dataGridView1.Columns["Проведён трезво"].DisplayIndex = 6;
			dataGridView1.Columns["Комментарий"].DisplayIndex = 7;
			
			dataGridView1.Columns["Обет/Промежуток"].Width = 102;
			dataGridView1.Columns["Место принятия"].Width = 112;
			dataGridView1.Columns["Проведён трезво"].Width = 102;
			
			if (tableType == 2)
			{
				lblInfo.Text = "При удалении записи о данном месте удалятся следующие записи об обетах (и промежутках между ними), принятых в этом месте:";
				lblQuestion.Text = "Вы действительно хотите удалить запись о данном месте и связанных с ним обетах?";
			}
		}
		
		void BtnNoClick(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
			this.Close();
		}
		
		void BtnYesClick(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Yes;
			this.Close();
		}
		
		void DelRowFormFormClosing(object sender, FormClosingEventArgs e)
		{
			if (this.DialogResult == DialogResult.None) this.DialogResult = DialogResult.Cancel;
		}
	}
}
