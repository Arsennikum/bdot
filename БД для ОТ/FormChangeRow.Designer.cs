﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Алексей
 * Дата: 06.06.2014
 * Время: 16:40
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
namespace БД_для_ОТ
{
	partial class FormChangeRow
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.pnlMInfo = new System.Windows.Forms.Panel();
			this.btnCancel1 = new System.Windows.Forms.Button();
			this.dtpMInfoBirthday = new System.Windows.Forms.DateTimePicker();
			this.btnMan = new System.Windows.Forms.Button();
			this.tbMInfoComment = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.tbMInfoEmail = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.tbMInfoTel = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.tbMInfoAddress = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.tbMInfoOName = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.tbMInfoName = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.tbMInfoFName = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.pnlPlaces = new System.Windows.Forms.Panel();
			this.btnPlace = new System.Windows.Forms.Button();
			this.tbPlaceComment = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.tbPlace = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.btnCancel2 = new System.Windows.Forms.Button();
			this.pnlIntervals = new System.Windows.Forms.Panel();
			this.btnInterval = new System.Windows.Forms.Button();
			this.tbIntervalsComment = new System.Windows.Forms.TextBox();
			this.cBoxIntervalsIsTrezv = new System.Windows.Forms.CheckBox();
			this.cbIntervalPlace = new System.Windows.Forms.ComboBox();
			this.label12 = new System.Windows.Forms.Label();
			this.cbIntervalType = new System.Windows.Forms.ComboBox();
			this.dtpDateEnd = new System.Windows.Forms.DateTimePicker();
			this.dtpDateBegin = new System.Windows.Forms.DateTimePicker();
			this.cbIntervalFIO = new System.Windows.Forms.ComboBox();
			this.label11 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label20 = new System.Windows.Forms.Label();
			this.btnCancel3 = new System.Windows.Forms.Button();
			this.pnlMInfo.SuspendLayout();
			this.pnlPlaces.SuspendLayout();
			this.pnlIntervals.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlMInfo
			// 
			this.pnlMInfo.Controls.Add(this.btnCancel1);
			this.pnlMInfo.Controls.Add(this.dtpMInfoBirthday);
			this.pnlMInfo.Controls.Add(this.btnMan);
			this.pnlMInfo.Controls.Add(this.tbMInfoComment);
			this.pnlMInfo.Controls.Add(this.label8);
			this.pnlMInfo.Controls.Add(this.label7);
			this.pnlMInfo.Controls.Add(this.tbMInfoEmail);
			this.pnlMInfo.Controls.Add(this.label6);
			this.pnlMInfo.Controls.Add(this.tbMInfoTel);
			this.pnlMInfo.Controls.Add(this.label5);
			this.pnlMInfo.Controls.Add(this.tbMInfoAddress);
			this.pnlMInfo.Controls.Add(this.label4);
			this.pnlMInfo.Controls.Add(this.tbMInfoOName);
			this.pnlMInfo.Controls.Add(this.label3);
			this.pnlMInfo.Controls.Add(this.tbMInfoName);
			this.pnlMInfo.Controls.Add(this.label2);
			this.pnlMInfo.Controls.Add(this.tbMInfoFName);
			this.pnlMInfo.Controls.Add(this.label1);
			this.pnlMInfo.Location = new System.Drawing.Point(12, 12);
			this.pnlMInfo.Name = "pnlMInfo";
			this.pnlMInfo.Size = new System.Drawing.Size(901, 88);
			this.pnlMInfo.TabIndex = 0;
			// 
			// btnCancel1
			// 
			this.btnCancel1.Location = new System.Drawing.Point(814, 54);
			this.btnCancel1.Name = "btnCancel1";
			this.btnCancel1.Size = new System.Drawing.Size(75, 23);
			this.btnCancel1.TabIndex = 35;
			this.btnCancel1.Text = "Отменить";
			this.btnCancel1.UseVisualStyleBackColor = true;
			this.btnCancel1.Click += new System.EventHandler(this.BtnCancelClick);
			// 
			// dtpMInfoBirthday
			// 
			this.dtpMInfoBirthday.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpMInfoBirthday.Location = new System.Drawing.Point(660, 28);
			this.dtpMInfoBirthday.Name = "dtpMInfoBirthday";
			this.dtpMInfoBirthday.Size = new System.Drawing.Size(100, 20);
			this.dtpMInfoBirthday.TabIndex = 34;
			// 
			// btnMan
			// 
			this.btnMan.Location = new System.Drawing.Point(734, 54);
			this.btnMan.Name = "btnMan";
			this.btnMan.Size = new System.Drawing.Size(75, 23);
			this.btnMan.TabIndex = 16;
			this.btnMan.Text = "Сохранить";
			this.btnMan.UseVisualStyleBackColor = true;
			this.btnMan.Click += new System.EventHandler(this.BtnManClick);
			// 
			// tbMInfoComment
			// 
			this.tbMInfoComment.Location = new System.Drawing.Point(766, 28);
			this.tbMInfoComment.Name = "tbMInfoComment";
			this.tbMInfoComment.Size = new System.Drawing.Size(123, 20);
			this.tbMInfoComment.TabIndex = 15;
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(766, 10);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(100, 15);
			this.label8.TabIndex = 14;
			this.label8.Text = "Комментарий:";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(660, 10);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(100, 15);
			this.label7.TabIndex = 12;
			this.label7.Text = "Дата рождения:";
			// 
			// tbMInfoEmail
			// 
			this.tbMInfoEmail.Location = new System.Drawing.Point(541, 28);
			this.tbMInfoEmail.Name = "tbMInfoEmail";
			this.tbMInfoEmail.Size = new System.Drawing.Size(113, 20);
			this.tbMInfoEmail.TabIndex = 11;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(541, 10);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(113, 15);
			this.label6.TabIndex = 10;
			this.label6.Text = "Электронная почта:";
			// 
			// tbMInfoTel
			// 
			this.tbMInfoTel.Location = new System.Drawing.Point(435, 28);
			this.tbMInfoTel.Name = "tbMInfoTel";
			this.tbMInfoTel.Size = new System.Drawing.Size(100, 20);
			this.tbMInfoTel.TabIndex = 9;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(435, 10);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(100, 15);
			this.label5.TabIndex = 8;
			this.label5.Text = "Телефон:";
			// 
			// tbMInfoAddress
			// 
			this.tbMInfoAddress.Location = new System.Drawing.Point(329, 28);
			this.tbMInfoAddress.Name = "tbMInfoAddress";
			this.tbMInfoAddress.Size = new System.Drawing.Size(100, 20);
			this.tbMInfoAddress.TabIndex = 7;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(329, 10);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(100, 15);
			this.label4.TabIndex = 6;
			this.label4.Text = "Адрес:";
			// 
			// tbMInfoOName
			// 
			this.tbMInfoOName.Location = new System.Drawing.Point(223, 28);
			this.tbMInfoOName.Name = "tbMInfoOName";
			this.tbMInfoOName.Size = new System.Drawing.Size(100, 20);
			this.tbMInfoOName.TabIndex = 5;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(223, 10);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(100, 15);
			this.label3.TabIndex = 4;
			this.label3.Text = "Отчество:";
			// 
			// tbMInfoName
			// 
			this.tbMInfoName.Location = new System.Drawing.Point(117, 28);
			this.tbMInfoName.Name = "tbMInfoName";
			this.tbMInfoName.Size = new System.Drawing.Size(100, 20);
			this.tbMInfoName.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(117, 10);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(100, 15);
			this.label2.TabIndex = 2;
			this.label2.Text = "Имя:";
			// 
			// tbMInfoFName
			// 
			this.tbMInfoFName.Location = new System.Drawing.Point(11, 28);
			this.tbMInfoFName.Name = "tbMInfoFName";
			this.tbMInfoFName.Size = new System.Drawing.Size(100, 20);
			this.tbMInfoFName.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(11, 10);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 15);
			this.label1.TabIndex = 0;
			this.label1.Text = "Фамилия:";
			// 
			// pnlPlaces
			// 
			this.pnlPlaces.Controls.Add(this.btnPlace);
			this.pnlPlaces.Controls.Add(this.tbPlaceComment);
			this.pnlPlaces.Controls.Add(this.label15);
			this.pnlPlaces.Controls.Add(this.tbPlace);
			this.pnlPlaces.Controls.Add(this.label16);
			this.pnlPlaces.Controls.Add(this.btnCancel2);
			this.pnlPlaces.Location = new System.Drawing.Point(12, 105);
			this.pnlPlaces.Name = "pnlPlaces";
			this.pnlPlaces.Size = new System.Drawing.Size(459, 87);
			this.pnlPlaces.TabIndex = 17;
			// 
			// btnPlace
			// 
			this.btnPlace.Location = new System.Drawing.Point(291, 54);
			this.btnPlace.Name = "btnPlace";
			this.btnPlace.Size = new System.Drawing.Size(75, 23);
			this.btnPlace.TabIndex = 16;
			this.btnPlace.Text = "Сохранить";
			this.btnPlace.UseVisualStyleBackColor = true;
			this.btnPlace.Click += new System.EventHandler(this.BtnPlaceClick);
			// 
			// tbPlaceComment
			// 
			this.tbPlaceComment.Location = new System.Drawing.Point(232, 28);
			this.tbPlaceComment.Name = "tbPlaceComment";
			this.tbPlaceComment.Size = new System.Drawing.Size(215, 20);
			this.tbPlaceComment.TabIndex = 3;
			// 
			// label15
			// 
			this.label15.Location = new System.Drawing.Point(232, 10);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(100, 15);
			this.label15.TabIndex = 2;
			this.label15.Text = "Комментарий:";
			// 
			// tbPlace
			// 
			this.tbPlace.Location = new System.Drawing.Point(11, 28);
			this.tbPlace.Name = "tbPlace";
			this.tbPlace.Size = new System.Drawing.Size(215, 20);
			this.tbPlace.TabIndex = 1;
			// 
			// label16
			// 
			this.label16.Location = new System.Drawing.Point(11, 10);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(215, 15);
			this.label16.TabIndex = 0;
			this.label16.Text = "Место, где принимают обет:";
			// 
			// btnCancel2
			// 
			this.btnCancel2.Location = new System.Drawing.Point(372, 54);
			this.btnCancel2.Name = "btnCancel2";
			this.btnCancel2.Size = new System.Drawing.Size(75, 23);
			this.btnCancel2.TabIndex = 36;
			this.btnCancel2.Text = "Отменить";
			this.btnCancel2.UseVisualStyleBackColor = true;
			this.btnCancel2.Click += new System.EventHandler(this.BtnCancelClick);
			// 
			// pnlIntervals
			// 
			this.pnlIntervals.Controls.Add(this.btnInterval);
			this.pnlIntervals.Controls.Add(this.tbIntervalsComment);
			this.pnlIntervals.Controls.Add(this.cBoxIntervalsIsTrezv);
			this.pnlIntervals.Controls.Add(this.cbIntervalPlace);
			this.pnlIntervals.Controls.Add(this.label12);
			this.pnlIntervals.Controls.Add(this.cbIntervalType);
			this.pnlIntervals.Controls.Add(this.dtpDateEnd);
			this.pnlIntervals.Controls.Add(this.dtpDateBegin);
			this.pnlIntervals.Controls.Add(this.cbIntervalFIO);
			this.pnlIntervals.Controls.Add(this.label11);
			this.pnlIntervals.Controls.Add(this.label14);
			this.pnlIntervals.Controls.Add(this.label13);
			this.pnlIntervals.Controls.Add(this.label9);
			this.pnlIntervals.Controls.Add(this.label20);
			this.pnlIntervals.Controls.Add(this.btnCancel3);
			this.pnlIntervals.Location = new System.Drawing.Point(12, 198);
			this.pnlIntervals.Name = "pnlIntervals";
			this.pnlIntervals.Size = new System.Drawing.Size(901, 87);
			this.pnlIntervals.TabIndex = 18;
			// 
			// btnInterval
			// 
			this.btnInterval.Location = new System.Drawing.Point(733, 53);
			this.btnInterval.Name = "btnInterval";
			this.btnInterval.Size = new System.Drawing.Size(75, 23);
			this.btnInterval.TabIndex = 57;
			this.btnInterval.Text = "Сохранить";
			this.btnInterval.UseVisualStyleBackColor = true;
			this.btnInterval.Click += new System.EventHandler(this.BtnIntervalClick);
			// 
			// tbIntervalsComment
			// 
			this.tbIntervalsComment.Location = new System.Drawing.Point(725, 27);
			this.tbIntervalsComment.Name = "tbIntervalsComment";
			this.tbIntervalsComment.Size = new System.Drawing.Size(164, 20);
			this.tbIntervalsComment.TabIndex = 55;
			// 
			// cBoxIntervalsIsTrezv
			// 
			this.cBoxIntervalsIsTrezv.Checked = true;
			this.cBoxIntervalsIsTrezv.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cBoxIntervalsIsTrezv.Location = new System.Drawing.Point(604, 27);
			this.cBoxIntervalsIsTrezv.Name = "cBoxIntervalsIsTrezv";
			this.cBoxIntervalsIsTrezv.Size = new System.Drawing.Size(115, 24);
			this.cBoxIntervalsIsTrezv.TabIndex = 54;
			this.cBoxIntervalsIsTrezv.Text = "Проведён трезво";
			this.cBoxIntervalsIsTrezv.UseVisualStyleBackColor = true;
			// 
			// cbIntervalPlace
			// 
			this.cbIntervalPlace.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbIntervalPlace.FormattingEnabled = true;
			this.cbIntervalPlace.Location = new System.Drawing.Point(265, 29);
			this.cbIntervalPlace.Name = "cbIntervalPlace";
			this.cbIntervalPlace.Size = new System.Drawing.Size(121, 21);
			this.cbIntervalPlace.TabIndex = 52;
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(138, 10);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(100, 19);
			this.label12.TabIndex = 49;
			this.label12.Text = "Тип промежутка:";
			// 
			// cbIntervalType
			// 
			this.cbIntervalType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbIntervalType.FormattingEnabled = true;
			this.cbIntervalType.Items.AddRange(new object[] {
									"Обет",
									"Промежуток между обетами"});
			this.cbIntervalType.Location = new System.Drawing.Point(138, 29);
			this.cbIntervalType.Name = "cbIntervalType";
			this.cbIntervalType.Size = new System.Drawing.Size(121, 21);
			this.cbIntervalType.TabIndex = 47;
			// 
			// dtpDateEnd
			// 
			this.dtpDateEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpDateEnd.Location = new System.Drawing.Point(498, 29);
			this.dtpDateEnd.Name = "dtpDateEnd";
			this.dtpDateEnd.Size = new System.Drawing.Size(100, 20);
			this.dtpDateEnd.TabIndex = 46;
			// 
			// dtpDateBegin
			// 
			this.dtpDateBegin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpDateBegin.Location = new System.Drawing.Point(392, 29);
			this.dtpDateBegin.Name = "dtpDateBegin";
			this.dtpDateBegin.Size = new System.Drawing.Size(100, 20);
			this.dtpDateBegin.TabIndex = 45;
			// 
			// cbIntervalFIO
			// 
			this.cbIntervalFIO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbIntervalFIO.FormattingEnabled = true;
			this.cbIntervalFIO.Location = new System.Drawing.Point(11, 29);
			this.cbIntervalFIO.Name = "cbIntervalFIO";
			this.cbIntervalFIO.Size = new System.Drawing.Size(121, 21);
			this.cbIntervalFIO.TabIndex = 44;
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(11, 10);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(100, 19);
			this.label11.TabIndex = 48;
			this.label11.Text = "ФИО:";
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(498, 10);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(100, 19);
			this.label14.TabIndex = 51;
			this.label14.Text = "Дата конца:";
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(392, 10);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(100, 16);
			this.label13.TabIndex = 50;
			this.label13.Text = "Дата начала:";
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(265, 10);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(100, 19);
			this.label9.TabIndex = 53;
			this.label9.Text = "Где принят:";
			// 
			// label20
			// 
			this.label20.Location = new System.Drawing.Point(725, 10);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(84, 19);
			this.label20.TabIndex = 56;
			this.label20.Text = "Комментарий:";
			// 
			// btnCancel3
			// 
			this.btnCancel3.Location = new System.Drawing.Point(814, 53);
			this.btnCancel3.Name = "btnCancel3";
			this.btnCancel3.Size = new System.Drawing.Size(75, 23);
			this.btnCancel3.TabIndex = 58;
			this.btnCancel3.Text = "Отменить";
			this.btnCancel3.UseVisualStyleBackColor = true;
			this.btnCancel3.Click += new System.EventHandler(this.BtnCancelClick);
			// 
			// FormChangeRow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(921, 293);
			this.Controls.Add(this.pnlIntervals);
			this.Controls.Add(this.pnlPlaces);
			this.Controls.Add(this.pnlMInfo);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "FormChangeRow";
			this.Text = "Изменение записи";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormChangeRowFormClosing);
			this.pnlMInfo.ResumeLayout(false);
			this.pnlMInfo.PerformLayout();
			this.pnlPlaces.ResumeLayout(false);
			this.pnlPlaces.PerformLayout();
			this.pnlIntervals.ResumeLayout(false);
			this.pnlIntervals.PerformLayout();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Button btnCancel3;
		private System.Windows.Forms.Button btnCancel2;
		private System.Windows.Forms.Button btnCancel1;
		private System.Windows.Forms.DateTimePicker dtpMInfoBirthday;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.ComboBox cbIntervalFIO;
		private System.Windows.Forms.DateTimePicker dtpDateBegin;
		private System.Windows.Forms.DateTimePicker dtpDateEnd;
		private System.Windows.Forms.ComboBox cbIntervalType;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.ComboBox cbIntervalPlace;
		private System.Windows.Forms.CheckBox cBoxIntervalsIsTrezv;
		private System.Windows.Forms.TextBox tbIntervalsComment;
		private System.Windows.Forms.Button btnInterval;
		private System.Windows.Forms.Panel pnlIntervals;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox tbPlace;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox tbPlaceComment;
		private System.Windows.Forms.Button btnPlace;
		private System.Windows.Forms.Panel pnlPlaces;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbMInfoFName;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tbMInfoName;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox tbMInfoOName;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox tbMInfoAddress;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox tbMInfoTel;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox tbMInfoEmail;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox tbMInfoComment;
		private System.Windows.Forms.Button btnMan;
		private System.Windows.Forms.Panel pnlMInfo;
	}
}
