﻿/*
 * Created by SharpDevelop.
 * User: Алексей
 * Date: 01.04.2016
 * Time: 18:08
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Data.OleDb;

namespace БД_для_ОТ
{
	/// <summary>
	/// Description of InsertCmdsCatalog.
	/// </summary>
	public static class InsertCmdsCatalog
	{
		
		public static OleDbCommand GetForManInfo(OleDbConnection conn) {
			var insManInfoCmd = new OleDbCommand("INSERT INTO ManInfo(Family, Name, O_Name, Birthday, Address, tel, Email, Comment)" +
			                                   "VALUES(@Family, @Name, @O_Name, @Birthday, @Address, @tel, @Email, @Comment)", conn);
			insManInfoCmd.Parameters.Add("@Family", OleDbType.VarChar, 50, "Фамилия");
			insManInfoCmd.Parameters.Add("@Name", OleDbType.VarChar, 50, "Имя");
			insManInfoCmd.Parameters.Add("@O_Name", OleDbType.VarChar, 50, "Отчество");
			insManInfoCmd.Parameters.Add("@Birthday", OleDbType.Date, 10, "Дата рождения");
			insManInfoCmd.Parameters.Add("@Address", OleDbType.VarChar, 100, "Адрес");
			insManInfoCmd.Parameters.Add("@tel", OleDbType.VarChar, 50, "Телефон");
			insManInfoCmd.Parameters.Add("@Email", OleDbType.VarChar, 50, "Электр.почта");
			insManInfoCmd.Parameters.Add("@Comment", OleDbType.VarChar, 250, "Комментарий");
			return insManInfoCmd;
		}
		
		public static OleDbCommand GetForIntervals(OleDbConnection conn) {
			OleDbCommand insIntervalsCmd = new OleDbCommand("INSERT INTO Intervals(id_man, id_place, DateBegin, DateEnd, is_obet, is_trezv, Comment)" +
			                                   "VALUES(@id_man, @id_place, @DateBegin, @DateEnd, @is_obet, @is_trezv, @Comment)",conn);
			insIntervalsCmd.Parameters.Add("@id_man", OleDbType.Integer, 6, "idMan");
			insIntervalsCmd.Parameters.Add("@id_place", OleDbType.Integer, 5, "idPlace");
			insIntervalsCmd.Parameters.Add("@DateBegin", OleDbType.Date, 10, "Начало");
			insIntervalsCmd.Parameters.Add("@DateEnd", OleDbType.Date, 10, "Конец");
			insIntervalsCmd.Parameters.Add("@is_obet", OleDbType.Boolean, 1, "Обет?");
			insIntervalsCmd.Parameters.Add("@is_trezv", OleDbType.Boolean, 1, "Проведён трезво");
			insIntervalsCmd.Parameters.Add("@Comment", OleDbType.VarChar, 250, "Комментарий");
			return insIntervalsCmd;
		}
		
		public static OleDbCommand GetForPlaces(OleDbConnection conn) {
			OleDbCommand insPlacesCmd = new OleDbCommand("INSERT INTO Places(Place, Comment)" +
			                                   "VALUES(@Place, @Comment)",conn);
			insPlacesCmd.Parameters.Add("@Place", OleDbType.VarChar, 100, "Place");
			insPlacesCmd.Parameters.Add("@Comment", OleDbType.VarChar, 250, "Комментарий");
			return insPlacesCmd;
		}
		
		public static OleDbCommand GetForAchievements(OleDbConnection conn) {
			OleDbCommand insAchievementCmd = new OleDbCommand("INSERT INTO Achievements(id_man, id_achieve_name, receive_date, comment)" +
			                                                  "VALUES(@id_man, @id_achievement, @receive_date, @comment)", conn);
			insAchievementCmd.Parameters.Add("@id_man", OleDbType.Integer, 6, "idMan");
			insAchievementCmd.Parameters.Add("@id_achieve_name", OleDbType.Integer, 6, "idAchieveName");
			insAchievementCmd.Parameters.Add("@receive_date", OleDbType.Integer, 6, "Дата вручения");
			insAchievementCmd.Parameters.Add("@comment", OleDbType.VarChar, 250, "Комментарий");
			return insAchievementCmd;
		}
		
		public static OleDbCommand GetForAchieveNames(OleDbConnection conn) {
			OleDbCommand insAchieveNamesCmd = new OleDbCommand("INSERT INTO AchieveNames(achieve_name, description)" +
			                                                "VALUES(@achieve_man, @description)", conn);
			insAchieveNamesCmd.Parameters.Add("@achieve_name", OleDbType.VarChar, 250, "Заголовок значка");
			insAchieveNamesCmd.Parameters.Add("@description", OleDbType.VarChar, 250, "Описание");
			return insAchieveNamesCmd;
		}
	}
}
