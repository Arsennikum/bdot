﻿/*
 * Created by SharpDevelop.
 * User: Алексей
 * Date: 31.03.2016
 * Time: 23:29
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Data.Common;

namespace БД_для_ОТ
{
	/// <summary>
	/// Description of TblMappingsCatalog.
	/// </summary>
	public static class TblMappingsCatalog
	{
		public static DataTableMapping GetForBasic()
		{
			DataColumnMapping[] dcm =
			{
				new DataColumnMapping("id", "id"),
				new DataColumnMapping("Family", "Фамилия"),
				new DataColumnMapping("Name", "Имя"),
				new DataColumnMapping("O_Name", "Отчество"),
				new DataColumnMapping("Place", "Место принятия"),
				new DataColumnMapping("DateBegin", "Начало"),
				new DataColumnMapping("DateEnd", "Конец"),
				//new DataColumnMapping("До окончания", "До окончания"),
				// new DataColumnMapping("is_obet", ""),
				new DataColumnMapping("is_trezv", "Проведён трезво"),
				new DataColumnMapping("Comment", "Комментарий"),
			};
			return new DataTableMapping("Table", "Basic", dcm);
		}
		
		public static DataTableMapping GetForManInfo()
		{
			DataColumnMapping[] dcm =
			{
				new DataColumnMapping("id", "id"),
				new DataColumnMapping("Family", "Фамилия"),
				new DataColumnMapping("Name", "Имя"),
				new DataColumnMapping("O_Name", "Отчество"),
				new DataColumnMapping("Birthday", "Дата рождения"),
				new DataColumnMapping("Address", "Адрес"),
				new DataColumnMapping("tel", "Телефон"),
				new DataColumnMapping("Email", "Электр.почта"),
				new DataColumnMapping("Comment", "Комментарий"),
			};
			return new DataTableMapping("Table", "ManInfo", dcm);
		}
		
		public static DataTableMapping GetForIntervals()
		{
			DataColumnMapping[] dcm =
			{
				new DataColumnMapping("id", "id"),
				new DataColumnMapping("id_man", "idMan"),
				new DataColumnMapping("id_place", "idPlace"),
				new DataColumnMapping("DateBegin", "Начало"),
				new DataColumnMapping("DateEnd", "Конец"),
				new DataColumnMapping("Is_obet", "Обет?"),
				new DataColumnMapping("is_trezv", "Проведён трезво"),
				new DataColumnMapping("Comment", "Комментарий"),
			};
			return new DataTableMapping("Table", "Intervals", dcm);
		}
		
		public static DataTableMapping GetForPlaces()
		{
			DataColumnMapping[] dcm =
			{
				new DataColumnMapping("id", "id"),
				new DataColumnMapping("Place", "Место"),
				new DataColumnMapping("Comment", "Комментарий"),
			};
			return new DataTableMapping("Table", "Places", dcm);
		}
		
		public static DataTableMapping GetForAchievements() {
			DataColumnMapping[] dcm = 
			{
				new DataColumnMapping("id", "id"),
				new DataColumnMapping("id_man", "idMan"),
				new DataColumnMapping("id_achieve_name", "idAchieveName"),
				new DataColumnMapping("receive_date", "Дата вручения"),
				new DataColumnMapping("comment", "Комментарий"),
			};
			return new DataTableMapping("Table", "Achievements", dcm);
		}
		
		public static DataTableMapping GetForAchieveName() {
			DataColumnMapping[] dcm = 
			{
				new DataColumnMapping("id", "id"),
				new DataColumnMapping("achieve_name", "Заголовок значка"),
				new DataColumnMapping("description", "Описание"),
			};
			return new DataTableMapping("Table", "AchieveNames", dcm);
		}
	}
}
