﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Алексей
 * Дата: 02.05.2014
 * Время: 16:10
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace БД_для_ОТ
{
	/// <summary>
	/// Основное окно программы. Большинство задач реализовано здесь.
	/// </summary>
	
	#region теория
	/*
 	* Так как символ бэкслэша является управляющим, то, если Вам необходимо передать в строке,
	* например, путь к файлу, то этот символ необходимо удвоить: C:\\windows\\file.txt
	* Если же удваивать слэш Вам не хочется или Вы не можете этого сделать, то можно передать строку так:
	* @”C:\\windows\file.txt”
	* Внутри такой строки управляющие символы и escape-последовательности работать не будут.
	* 
	* DataRelation - это всегда отношение "один ко многим". 
			чтобы реализовать отношение "один к одному" нужно добавить 
			в child таблицу UniqueConstraint или PrimaryKey на это поле.
			при добавлении DataRelation автоматически добавляется UniqueConstraint 
			на поле(я) в parent таблице (если не указано false при создании 
			DataRelation в параметре CreateConstraints).
	*/
	#endregion
	
	#region примечания
		/*
		 * Чтобы запретить изменение ячейки пользователем, попробуй dgv.EditMode = EditProgrammatically 
		 * или для столбцов и/или ячеек установи ReadOnly
		 * только смотри, лучше чтобы можно было копировать текст из ячейки, но не изменять
		 * 
		 * Замени названия таблиц на цифры - чтобы быстрее // для масштабирования не стоит. Разве что в крайних случаях
		 * 
		 * Выводить id по порядку, не как в базе, а выводя, для подсчёта.
		 */
	#endregion
	
	#region идеи для развития
	/*
	 * Для развития до дипломного, до ИС...
	 * по выведенным ближайшим дням рождения сформировать открытки для рассылки по электронной почте:
	 * Комплект ~вордовских документов, со стандартным текстом и оформлением 
	 * (задаваемым в настройках, например, шаблонным документом с маской),
	 * где программа вставляет на место маски ФИО человека.
	 * 
	 * Также по юбилеям жизни в обете - документ поздравление.
	 * 
	 * У каждого человека задавать фото. Допустим, папка, где они с индексами и по кнопке выводить фотографию человека
	 * 
	 * Сделать дополнительную программу, в которой можно выбрать людей в базе данных - и она выделит их и их обеты 
	 * в отдельную базу данных (галочка - выделять ли места в отдельную, если они были только у этих людей, иначе - скопировать). 
	 * Также в ней можно сделать для мест (отмечать места, а не людей).
	 * 
	 * Отображать интервал человека, где он не нарушал
	 * 
	 * Прикрутить события RowChange, RowDelete, TableNewRow у ds.Tables[], чтобы в них делать Update и IDENT_CURRENT('table_name')
	 * код будет читабельней и масштабировать легче. А то щас костыли крутить буду
	 */
	#endregion
	
	#region Для хелпа
	/*
	 * Пояснение, что кнопки отобразить действующие обеты на день и заканчивающиеся скоро действуют только для обетов, но не для промежутков
	 * 
	 * Пояснение, что при наведении на ячейку всплывает подсказка с полным значением, если оно не влезло. Также можно столбец расширить.
	 * 
	 * Можно скопировать значение из столбца
	 * 
	 */
	#endregion
	
	public class ProgSettings
	{
		public string dbPath;
		public string BackupPath;
		public string patternPath;
	}
	
	public partial class MainForm : Form
	{
		//String connStr = @"Data Source=(local); Initial Catalog= SocietyOfSobriety; integrated security=true;";
		readonly String settingsPath = "Settings.xml"; //Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "БД_для_ОТ/Settings.xml");
		const String connStr = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = "; // Persist Security Info = False;"; // или Database Password = 123";
		/*
		String SQLselectBasicString = "SELECT " +
			"DISTINCT MI.id, Family, Name, O_Name, Places.Place, DateBegin, DateEnd, is_trezv, Intervals.Comment, Places.Id AS idPlace " +
			"FROM Intervals " +
			"INNER JOIN ManInfo AS MI ON MI.Id = Intervals.Id_Man " +
			"INNER JOIN Places ON Intervals.id_place = Places.id " +
			"WHERE (is_obet = 'true') AND (DateEnd = (SELECT MAX(DateEnd) FROM Intervals WHERE (id_man = MI.id) AND (is_obet = 'TRUE')))";
			*/
		const String selectBasicString = "SELECT " +
			"DISTINCT MI.id, Family, Name, O_Name, Places.Place, DateBegin, DateEnd, is_trezv, Intervals.Comment, Places.Id AS idPlace " +
			"FROM ((Intervals " +
			"INNER JOIN ManInfo AS MI ON MI.Id = Intervals.Id_Man) " +
			"INNER JOIN Places ON Intervals.id_place = Places.id) " +
			"WHERE (is_obet = true) AND (DateEnd = (SELECT MAX(DateEnd) FROM Intervals WHERE (id_man = MI.id) AND (is_obet = TRUE)))";
//		String backupStrArg0 = "BACKUP DATABASE [SocietyOfSobriety] TO  DISK = N'{0}' " +
//			"WITH NOFORMAT, NOINIT, NAME = N'SocietyOfSobriety-Полная База данных Резервное копирование', " +
//			"SKIP, NOREWIND, NOUNLOAD,  STATS = 10, CHECKSUM";

			/*"BACKUP DATABASE [SocietyOfSobriety] " +
			"TO  DISK = N'{0}' " +
			"WITH NOFORMAT, NOINIT,  NAME = N'SocietyOfSobriety-Полная База данных Резервное копирование', " +
			"SKIP, NOREWIND, NOUNLOAD,  STATS = 10, CHECKSUM \n GO \n declare @backupSetId as int " +
			"select @backupSetId = position from msdb..backupset where database_name=N'SocietyOfSobriety' " +
			"and backup_set_id=(select max(backup_set_id) from msdb..backupset where database_name=N'SocietyOfSobriety' ) " +
			"if @backupSetId is null begin " +
			"raiserror(N'Ошибка верификации. Сведения о резервном копировании для базы данных \"SocietyOfSobriety\" не найдены.', 16, 1) end " +
			"RESTORE VERIFYONLY FROM  DISK = N'{0}' " +
			"WITH  FILE = @backupSetId,  NOUNLOAD,  NOREWIND \n GO";*/
	
		OleDbConnection conn;
		OleDbDataAdapter daBasic;
		OleDbDataAdapter daManInfo;
		OleDbDataAdapter daIntervals;
		OleDbDataAdapter daPlaces;
		OleDbDataAdapter daAchievements;
		OleDbDataAdapter daAchieveNames;
		
		/*SqlConnection connI;
		SqlDataAdapter daBasicI;
		SqlDataAdapter daManInfoI;
		SqlDataAdapter daIntervalsI;
		SqlDataAdapter daPlacesI;*/
		DataSet ds;
		
		DataView dvIntervals;
		DataView dvBasic;
		
		readonly ProgSettings settings = new ProgSettings();
		
		/*public enum ST
		{
			SqlServ,
			Access,
		}
		
		ST ServType = ST.Access;
		
		public ST ServerType
		{
			get
			{
				return ServType;
			}
			set
			{
				ServType = value;
			}
		}*/
		
		void AddBtnsToDgv(DataGridView dgv)
		{
			DataGridViewButtonColumn chnge = new DataGridViewButtonColumn();
			chnge.UseColumnTextForButtonValue = true;
			chnge.Name = "Изменить";
			chnge.Text = "Изменить";
			chnge.DisplayIndex = dgv.Columns.Count;
			dgv.Columns.Add(chnge);
			//dgv.Columns["Изменить"].DisplayIndex = dgv.Columns.Count-1;
			DataGridViewButtonColumn delBut = new DataGridViewButtonColumn();
			delBut.UseColumnTextForButtonValue = true;
			delBut.Name = "Удалить";
			delBut.Text = "Удалить";
			delBut.DisplayIndex = dgv.Columns.Count;
			dgv.Columns.Add(delBut);
			//dgv.Columns["Удалить"].DisplayIndex = dgv.Columns.Count-1;
		}
		
		int GetIncSeed(string TableName)
		{
			OleDbCommand ident_current = new OleDbCommand(String.Format("select CAST(ident_current('{0}') AS int)", TableName), conn);
			conn.Open();
			int? res = ident_current.ExecuteScalar() as int?;
			conn.Close();
			if (res == null)
			{
				MessageBox.Show("Произошла ошибка при запросе последнего идентификатора (функция IDENT_CURRENT). " +
				                "Будет возвращено значение 0. Попробуйте вставить строку в каждую таблицу и перезапустить программу.",
				                "Ошибка IDENT_CURRENT", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
				return 0;
			}
			return (int)res;
		}
		
		void MainLoad()
		{
			// Объявление
			ds = new DataSet();
			conn = new OleDbConnection(connStr + settings.dbPath);
			daBasic = new OleDbDataAdapter(selectBasicString, conn);
			daManInfo = new OleDbDataAdapter("SELECT * FROM ManInfo", conn);
			daIntervals = new OleDbDataAdapter("SELECT * FROM Intervals", conn);
			daPlaces = new OleDbDataAdapter("SELECT * FROM Places", conn);
			daAchievements = new OleDbDataAdapter("SELECT * FROM Achievements", conn);
			daAchieveNames = new OleDbDataAdapter("SELECT * FROM AchieveNames", conn);
			
			// Задание DateTableMapping
			daBasic.TableMappings.Add(TblMappingsCatalog.GetForBasic());
			daManInfo.TableMappings.Add(TblMappingsCatalog.GetForManInfo());
			daIntervals.TableMappings.Add(TblMappingsCatalog.GetForIntervals());
			daPlaces.TableMappings.Add(TblMappingsCatalog.GetForPlaces());
			daAchievements.TableMappings.Add(TblMappingsCatalog.GetForAchievements());
			daAchieveNames.TableMappings.Add(TblMappingsCatalog.GetForAchieveName());
			
			#region --- Заполнение DataSet и DataGridView'ов, задание InsertCommand для DataAdapter'ов ---
			
				// Таблица ManInfo
			daManInfo.Fill(ds);
			daManInfo.InsertCommand = InsertCmdsCatalog.GetForManInfo(conn);
			dgvManInfo.DataSource = new DataView(ds.Tables["ManInfo"]);
			
				// Таблица Intervals
			daIntervals.Fill(ds);
			dvIntervals = new DataView(ds.Tables["Intervals"]);
			LoadBasic(true);
			dgvIntervals.DataSource = new DataView(ds.Tables["Basic"]);
			daIntervals.InsertCommand = InsertCmdsCatalog.GetForIntervals(conn);
			
				// Таблица Places
			daPlaces.Fill(ds);
			dgvPlaces.DataSource = new DataView(ds.Tables["Places"]);
			daPlaces.InsertCommand = InsertCmdsCatalog.GetForPlaces(conn);
			
				// Таблица Achievements
			daAchievements.Fill(ds);
			dgvAchievements.DataSource = new DataView(ds.Tables["Achievements"]);
			daAchievements.InsertCommand = InsertCmdsCatalog.GetForAchievements(conn);
			
				// Таблица AchieveNames
			daAchieveNames.Fill(ds);
			dgvAchieveNames.DataSource = new DataView(ds.Tables["AchieveNames"]);
			daAchieveNames.InsertCommand = InsertCmdsCatalog.GetForAchieveNames(conn);
			
			
				// Ещё нужно прописывать UpdateCommand для каждого. Лучше задам автоматически
			// Почему-то ручное удаление тоже особо не работало - закомментил. 
			// Да работало оно. Видимо опять хотел удалить после создания, когда не задан id
			OleDbCommandBuilder cbMI = new OleDbCommandBuilder(daManInfo);
			OleDbCommandBuilder cbP = new OleDbCommandBuilder(daPlaces);
			OleDbCommandBuilder cbI = new OleDbCommandBuilder(daIntervals);
			OleDbCommandBuilder cbA = new OleDbCommandBuilder(daAchievements);
			OleDbCommandBuilder cbAN = new OleDbCommandBuilder(daAchieveNames);
			
			#endregion
			
			// Заполнение ComboBox'ов
			ds.Tables["ManInfo"].Columns.Add("FIO", typeof(string), "Фамилия + ' ' + Имя");
			dgvManInfo.Columns["FIO"].Visible = false;
			cbIntervalFIO.DataSource = ds.Tables["ManInfo"];
			cbIntervalFIO.DisplayMember = "FIO";
			cbIntervalFIO.ValueMember = "id";
			cbIntervalFIO.BindingContext = new BindingContext();
			cbIntervalFIO.SelectedIndex = -1;
			cbIntervalPlace.DataSource = ds.Tables["Places"];
			cbIntervalPlace.DisplayMember = "Место";
			cbIntervalPlace.ValueMember = "id";
			cbIntervalPlace.BindingContext = new BindingContext();
			cbIntervalPlace.SelectedIndex = -1;
			
			// Для оформления (id заменяются на текст и др)
			cbIntervalType.SelectedIndex = 0;
			ds.Relations.Add(new DataRelation("MansIntervals", 
			                                  ds.Tables["ManInfo"].Columns["id"],
			                                  ds.Tables["Intervals"].Columns["idMan"]));
			ds.Tables["Intervals"].Columns.Add("ФИО", typeof(string), "Parent(MansIntervals).FIO");
			
			ds.Relations.Add(new DataRelation("PlacesIntervals",
			                              ds.Tables["Places"].Columns["id"],
			                              ds.Tables["Intervals"].Columns["idPlace"]));
			ds.Tables["Intervals"].Columns.Add("Место принятия", typeof(string), "Parent(PlacesIntervals).Место");
			
			ds.Tables["Intervals"].Columns.Add("Обет/Промежуток", typeof(string), "IIF([Обет?] = true, 'Обет', 'Промежуток между обетами')");
			
			ds.Relations.Add(new DataRelation("MansAchievements",
			                                  ds.Tables["ManInfo"].Columns["id"],
			                                  ds.Tables["Achievements"].Columns["idMan"]));
			ds.Tables["Achievements"].Columns.Add("ФИО", typeof(string), "Parent(MansAchievements).FIO");
			
			ds.Relations.Add(new DataRelation("AchieveNameAchievements",
			                      ds.Tables["AchieveNames"].Columns["id"],
			                      ds.Tables["Achievements"].Columns["idAchieveName"]));
			ds.Tables["Achievements"].Columns.Add("Значок", typeof(string), "Parent(AchieveNameAchievements).[Заголовок значка]");
			
			ds.Tables["ManInfo"].Columns.Add("№ добавления", typeof(string), "id");
			ds.Tables["Intervals"].Columns.Add("№ добавления", typeof(string), "id");
			ds.Tables["Places"].Columns.Add("№ добавления", typeof(string), "id");
			ds.Tables["Achievements"].Columns.Add("№ добавления", typeof(string), "id");
			
			for (int i = 1; i<=31; i++) {
				cbDayFrom.Items.Add(i);
				cbDayTo.Items.Add(i);
			}
			cbDayFrom.SelectedIndex = 0;
			cbDayTo.SelectedIndex = 30;
			List<KeyValuePair<int, String>> Months = new List<KeyValuePair<int, string>>
			{
				new KeyValuePair<int, string>(1, "Январь"),
				new KeyValuePair<int, string>(2, "Февраль"),
				new KeyValuePair<int, string>(3, "Март"),
				new KeyValuePair<int, string>(4, "Апрель"),
				new KeyValuePair<int, string>(5, "Май"),
				new KeyValuePair<int, string>(6, "Июнь"),
				new KeyValuePair<int, string>(7, "Июль"),
				new KeyValuePair<int, string>(8, "Август"),
				new KeyValuePair<int, string>(9, "Сентябрь"),
				new KeyValuePair<int, string>(10, "Октябрь"),
				new KeyValuePair<int, string>(11, "Ноябрь"),
				new KeyValuePair<int, string>(12, "Декабрь"),
			};
			cbMonthFrom.DataSource = Months;
			cbMonthFrom.DisplayMember = "Value";
			cbMonthFrom.ValueMember = "Key";
			cbMonthFrom.BindingContext = new BindingContext();
			cbMonthTo.DataSource = Months;
			cbMonthTo.DisplayMember = "Value";
			cbMonthTo.ValueMember = "Key";
			cbMonthTo.SelectedValue = 12;
			
			
			// Задание автоинкремента для id'шников, чтобы удалялось и изменялось только что добавленное
//			if (ServType == ST.SqlServ)
//			{
//				ds.Tables["ManInfo"].Columns["id"].AutoIncrement = true;
//				ds.Tables["ManInfo"].Columns["id"].AutoIncrementSeed = GetIncSeed("ManInfo") + 1;
//				ds.Tables["Places"].Columns["id"].AutoIncrement = true;
//				ds.Tables["Places"].Columns["id"].AutoIncrementSeed = GetIncSeed("Places") + 1;
//				ds.Tables["Intervals"].Columns["id"].AutoIncrement = true;
//				ds.Tables["Intervals"].Columns["id"].AutoIncrementSeed = GetIncSeed("Intervals") + 1;
//			}
//			else
//			{
				daManInfo.RowUpdated += new OleDbRowUpdatedEventHandler(da_OnRowUpdate);
				daPlaces.RowUpdated += new OleDbRowUpdatedEventHandler(da_OnRowUpdate);
				daIntervals.RowUpdated += new OleDbRowUpdatedEventHandler(da_OnRowUpdate);
				daAchievements.RowUpdated += new OleDbRowUpdatedEventHandler(da_OnRowUpdate);
//			}
			
			// Добавление кнопок изменить/удалить в таблицы
			AddBtnsToDgv(dgvManInfo);
			AddBtnsToDgv(dgvPlaces);
			AddBtnsToDgv(dgvIntervals);
			AddBtnsToDgv(dgvAchievements);
			
			// Настрока отображения столбцов
			dgvIntervals.AutoGenerateColumns = false;
			dgvIntervals.Columns["id"].Visible = false;
			dgvIntervals.Columns["Изменить"].Visible = false;
			dgvIntervals.Columns["Удалить"].Visible = false;
			dgvIntervals.Columns["id"].Visible = false;
			dgvIntervals.Columns["idPlace"].Visible = false;
			dgvIntervals.Columns["Фамилия"].DisplayIndex = 0;
			dgvIntervals.Columns["Имя"].DisplayIndex = 1;
			dgvIntervals.Columns["Отчество"].DisplayIndex = 2;
			dgvIntervals.Columns["Место принятия"].DisplayIndex = 3;
			dgvIntervals.Columns["Начало"].DisplayIndex = 4;
			dgvIntervals.Columns["Конец"].DisplayIndex = 5;
			dgvIntervals.Columns["Проведён трезво"].DisplayIndex = 6;
			dgvIntervals.Columns["Комментарий"].DisplayIndex = 7;
			dgvIntervals.Columns["Изменить"].DisplayIndex = 8;
			dgvIntervals.Columns["Удалить"].DisplayIndex = 9;
			dgvIntervals.Columns["Место принятия"].Width = 112;
			dgvIntervals.Columns["Проведён трезво"].Width = 102;
			dgvIntervals.AutoGenerateColumns = true;
			
			dgvAchievements.AutoGenerateColumns = false;
			dgvAchievements.Columns["№ добавления"].DisplayIndex = 0;
			dgvAchievements.Columns["ФИО"].DisplayIndex = 1;
			dgvAchievements.Columns["Значок"].DisplayIndex = 2;
			dgvAchievements.Columns["Дата вручения"].DisplayIndex = 3;
			dgvAchievements.Columns["Комментарий"].DisplayIndex = 4;
			dgvAchievements.Columns["Изменить"].DisplayIndex = 5;
			dgvAchievements.Columns["Удалить"].DisplayIndex = 6;
			dgvAchievements.Columns["id"].Visible = false;
			dgvAchievements.Columns["idMan"].Visible = false;
			dgvAchievements.Columns["idAchieveName"].Visible = false;
			
			dgvPlaces.AutoGenerateColumns = false;
			dgvPlaces.Columns["№ добавления"].DisplayIndex = 0;
			dgvPlaces.Columns["Место"].DisplayIndex = 1;
			dgvPlaces.Columns["Комментарий"].DisplayIndex = 2;
			dgvPlaces.Columns["Изменить"].DisplayIndex = 3;
			dgvPlaces.Columns["Удалить"].DisplayIndex = 4;
			dgvPlaces.Columns["id"].Visible = false;
			
			dgvManInfo.AutoGenerateColumns = false;
			dgvManInfo.Columns["id"].Visible = false;
			dgvManInfo.Columns["№ добавления"].DisplayIndex = 0;
			dgvManInfo.Columns["Фамилия"].DisplayIndex = 1;
			dgvManInfo.Columns["Имя"].DisplayIndex = 2;
			dgvManInfo.Columns["Отчество"].DisplayIndex = 3;
			dgvManInfo.Columns["Дата рождения"].DisplayIndex = 4;
			dgvManInfo.Columns["Адрес"].DisplayIndex = 5;
			dgvManInfo.Columns["Телефон"].DisplayIndex = 6;
			dgvManInfo.Columns["Электр.почта"].DisplayIndex = 7;
			dgvManInfo.Columns["Комментарий"].DisplayIndex = 8;
			dgvManInfo.Columns["Изменить"].DisplayIndex = 9;
			dgvManInfo.Columns["Удалить"].DisplayIndex = 10;
			dgvManInfo.Columns["№ добавления"].Width = 40;
			dgvManInfo.Columns["Дата рождения"].Width = 109;
			
			dgvAchieveNames.Columns["id"].Visible = false;
			dgvAchieveNames.Columns["Заголовок значка"].Width = 130;
			dgvAchieveNames.Columns["Описание"].Width = 250;
		}
		/// <summary>
		/// Загружает или перезагружает DataColumn "Basic"
		/// </summary>
		void LoadBasic(bool firstLoad)
		{
			if (firstLoad)// Для производительности (!ds.Tables.Contains("Basic"))
			{
				daBasic.Fill(ds);
				ds.Tables["Basic"].Columns.Add("До конца", typeof(string));
			}
			else
			{
				ds.Tables["Basic"].Clear();
				daBasic.Fill(ds);
			}
			var daysCount = from t in ds.Tables["Basic"].AsEnumerable()
					select ((DateTime)t["Конец"]).Date.Subtract(DateTime.Today).Days;
			int i = 0;
			foreach(var t in daysCount)
			{
				ds.Tables["Basic"].Rows[i]["До конца"] = t < 0 ? "Закончился" : t.ToString() + " дней";
				i++;
			}
			
		}
		
		void MainLoad_Wrapper()
		{
			MainLoad();
			try
			{
				//MainLoad(); TODO:
			} catch (Exception e) {
				tabControl1.SelectTab("tabSettings");
				MessageBox.Show("Произошла ошибка при загрузки приложения. Попробуйте изменить настройки"+Environment.NewLine+
				                "Текст ошибки: " + Environment.NewLine + e.Message + Environment.NewLine + e.StackTrace,
				                "Ошибка загрузки", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
				// TODO: по-хорошему отключить кнопки "добавить" и другие, которые вызывают ошибку. А после MainLoad() включать
			}
		}
		
		public MainForm()
		{
			InitializeComponent();
			// Для оформления
			lblSaveSettingsStatus.Text = "";
			lblBackupStatus.Text = "";
			lblSyncStatus.Text = "";
			
			// Загрузка настроек программы из xml файла
			if (!System.IO.File.Exists(settingsPath))
			{
				settings.dbPath = "SocietyOfSobriety.accdb"; //@"C:\db\SocietyOfSobriety.accdb";
				settings.BackupPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\БД для ОТ\backups\";
				settings.patternPath = ""; //Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\БД_для_ОТ\";
				if (settingsPath != "Settings.xml") System.IO.Directory.CreateDirectory(Path.GetDirectoryName(settingsPath));
				SaveSettings();
				// Чтобы для начала открыть вкладку для заполнения людей.
				tabControl1.SelectTab(1);
			}
			else
			{
				using (Stream stream = new FileStream(settingsPath, FileMode.Open))
				{
					XmlSerializer serializer = new XmlSerializer(typeof(ProgSettings));
					settings = (ProgSettings)serializer.Deserialize(stream);
				}
			}
			tbDBPath.Text = settings.dbPath;
			tbBackupPath.Text = settings.BackupPath;
			
			MainLoad_Wrapper();
		}
		
		static void da_OnRowUpdate(object sender, OleDbRowUpdatedEventArgs  e)
		{
			// Видимо записывает изменения в базу при добавлении строки
			if (e.StatementType == StatementType.Insert)
			{
				OleDbCommand oCmd = new OleDbCommand("SELECT @@IDENTITY", e.Command.Connection);
				e.Row["id"] = oCmd.ExecuteScalar();
				e.Row.AcceptChanges();
			}
		}
		
		void TbInputEnter(object sender, EventArgs e)
		{
			TextBox thisTB = sender as TextBox;
			if (thisTB.Text == thisTB.Tag as String) 
			{
				thisTB.Text = "";
				thisTB.ForeColor = SystemColors.WindowText;
			}
		}
		
		void TbInputLeave(object sender, EventArgs e)
		{
			TextBox thisTB = sender as TextBox;
			if (thisTB.Text == "") 
			{
				thisTB.Text = thisTB.Tag as String;
				thisTB.ForeColor = SystemColors.WindowFrame;
			}
		}
		
		void TbBirthdayEnter(object sender, EventArgs e)
		{
			tbBirthday.Visible = false;
			dtpBirthday.Focus();
		}
		
		void BtnGetBirthdayClick(object sender, EventArgs e)
		{ 
			/* Быть не может!
 			if (((cbMonthFrom.SelectedValue as int?) == -1)||((cbMonthTo.SelectedValue as int?) == -1)||
			    (cbDayFrom.SelectedIndex == -1)||(cbDayTo.SelectedIndex == -1))
			{
				MessageBox.Show("Вы выбрали не все данные. Выберите и повторите");
				return;
			}*/
			// yyyy-MM-dd
			(dgvManInfo.DataSource as DataView).RowFilter = 
				String.Format("([Дата рождения] >= CONVERT(SUBSTRING(CONVERT([Дата рождения], System.String), 7, 4) + '-{0}-{1}', System.DateTime)) " +
				              "AND ([Дата рождения] <= CONVERT(SUBSTRING(CONVERT([Дата рождения], System.String), 7, 4) + '-{2}-{3}', System.DateTime))",
				              cbMonthFrom.SelectedValue, cbDayFrom.Text, cbMonthTo.SelectedValue, cbDayTo.Text);
			
			// Если успешно
			lblManInfoStatus.Text = String.Format("Отображаются люди, у которых день рождения от '{0} {1}' до '{2} {3}'",
			                                      cbDayFrom.Text, cbMonthFrom.Text, cbDayTo.Text, cbMonthTo.Text);
			lblManInfoStatus.Font = new Font(lblManInfoStatus.Font, FontStyle.Italic);
			btnGetAllManInfo.Visible = true;
		}
		
		void BtnGetAllManInfoClick(object sender, EventArgs e)
		{
			(dgvManInfo.DataSource as DataView).RowFilter = "";
			// Если успешно
			lblManInfoStatus.Text = "Здесь отображается информация о людях, можно добавить информацию и просмотреть ближайшие дни рождения";
			lblManInfoStatus.Font = new Font(lblManInfoStatus.Font, FontStyle.Bold);
			btnGetAllManInfo.Visible = false;
		}
		
		void BtnGetNearVowClick(object sender, EventArgs e)
		{
			dgvIntervals.DataSource = dvIntervals;
			dvIntervals.RowFilter = String.Format("(Конец >= '{0}') AND (Конец <= '{1}') AND ([Обет?] = true)",
			                             dtpNearVowFrom.Value.ToString("yyyy-MM-dd"), dtpNearVowTo.Value.ToString("yyyy-MM-dd"));
			
			// Если успешно
			
			cbIntervalFIO.Enabled = true;
			cbIntervalFIO.SelectedValue = -1;
			
			lblIntervalsStatus.Text = String.Format("Отображаются люди, у которых заканчивается обет с {0} по {1}",
			                                      dtpNearVowFrom.Value.ToString("dd.MM.yyyy"),
			                                      dtpNearVowTo.Value.ToString("dd.MM.yyyy"));
			lblIntervalsStatus.Font = new Font(lblManInfoStatus.Font, FontStyle.Italic);
			btnGetAllIntervals.Visible = true;
			btnGetManVows.Visible = false;
			
			dgvIntervals.Columns["idMan"].Visible = false;
			dgvIntervals.Columns["idPlace"].Visible = false;
			dgvIntervals.Columns["Обет?"].Visible = false;
			dgvIntervals.Columns["Изменить"].Visible = true;
			dgvIntervals.Columns["Удалить"].Visible = true;
			dgvIntervals.Columns["ФИО"].DisplayIndex = 0;
			dgvIntervals.Columns["Обет/Промежуток"].DisplayIndex = 1;
			dgvIntervals.Columns["Место принятия"].DisplayIndex = 3;
			dgvIntervals.Columns["Начало"].DisplayIndex = 4;
			dgvIntervals.Columns["Конец"].DisplayIndex = 5;
			dgvIntervals.Columns["Проведён трезво"].DisplayIndex = 6;
			dgvIntervals.Columns["Комментарий"].DisplayIndex = 7;
			dgvIntervals.Columns["Изменить"].DisplayIndex = 9;
			dgvIntervals.Columns["Удалить"].DisplayIndex = 10;
			dgvIntervals.Columns["Обет/Промежуток"].Width = 102;
		}
		
		void BtnGetInVowClick(object sender, EventArgs e)
		{
			dgvIntervals.DataSource = dvIntervals;
			dvIntervals.RowFilter = String.Format("(Начало <= '{0}') AND (Конец >= '{0}') AND ([Обет?] = true)",
			                                                              dtpInVow.Value.ToString("yyyy-MM-dd"));
			
			// Если успешно
			cbIntervalFIO.Enabled = true;
			cbIntervalFIO.SelectedValue = -1;
			lblIntervalsStatus.Text = String.Format("Отображаются люди с действующим обетом на {0}",
			                                    dtpInVow.Value.ToString("dd.MM.yyyy"));
			lblIntervalsStatus.Font = new Font(lblIntervalsStatus.Font, FontStyle.Italic);
			btnGetAllIntervals.Visible = true;
			btnGetManVows.Visible = false;
			
			dgvIntervals.Columns["idMan"].Visible = false;
			dgvIntervals.Columns["idPlace"].Visible = false;
			dgvIntervals.Columns["Обет?"].Visible = false;
			dgvIntervals.Columns["Изменить"].Visible = true;
			dgvIntervals.Columns["Удалить"].Visible = true;
			dgvIntervals.Columns["ФИО"].DisplayIndex = 0;
			dgvIntervals.Columns["Обет/Промежуток"].DisplayIndex = 1;
			dgvIntervals.Columns["Место принятия"].DisplayIndex = 3;
			dgvIntervals.Columns["Начало"].DisplayIndex = 4;
			dgvIntervals.Columns["Конец"].DisplayIndex = 5;
			dgvIntervals.Columns["Проведён трезво"].DisplayIndex = 6;
			dgvIntervals.Columns["Комментарий"].DisplayIndex = 7;
			dgvIntervals.Columns["Изменить"].DisplayIndex = 9;
			dgvIntervals.Columns["Удалить"].DisplayIndex = 10;
			dgvIntervals.Columns["Обет/Промежуток"].Width = 102;
		}
		
		#region add entry buttons
		void BtnAddManClick(object sender, EventArgs e)
		{
			// (Family, Name, O_Name, Birthday, Address, tel, Email, Comment)
			if (tbFName.ForeColor == SystemColors.WindowFrame // HACK
			    && tbName.ForeColor == SystemColors.WindowFrame)
			    //&& tbOName.ForeColor == SystemColors.WindowFrame)
			{
				lblAddManStatus.Text = "Вы не ввели ФИО. Добавление не произведено";
				lblAddManStatus.ForeColor = Color.Red;
				return;
			}
			lblAddManStatus.ForeColor = Color.Black;
			DataRow dr = ds.Tables["ManInfo"].NewRow();
			dr[1] = tbFName.Text.Equals("Фамилия") ? "" : tbFName.Text; // FIXME: повторная проверка (выше на цвет проверяю..) Но оставил для надежности при изменениях
			dr[2] = tbName.Text.Equals("Имя") ? "" : tbName.Text;
			dr[3] = tbOName.Text.Equals("Отчество") ? "" : tbOName.Text;
			dr[4] = dtpBirthday.Value.ToString("yyyy-MM-dd"); 
			dr[5] = tbAddress.Text.Equals("Адрес") ? "" : tbAddress.Text;
			dr[6] = tbTel.Text.Equals("Телефон") ? "" : tbTel.Text;// TODO: здесь для скорости можно сделать hack из начала метода
			dr[7] = tbEmail.Text.Equals("Электронная почта") ? "" : tbEmail.Text;
			dr[8] = tbManComment.Text.Equals("Комментарий") ? "" :tbManComment .Text;
			ds.Tables["ManInfo"].Rows.Add(dr);
			if (daManInfo.Update(ds.Tables["ManInfo"])==-1)
				lblAddManStatus.Text = String.Format("Добавление \"{0} {1}\" не удалось", tbFName.Text, tbName.Text);
			else
			{
				lblAddManStatus.Text = String.Format("Добавление \"{0} {1}\" успешно", tbFName.Text, tbName.Text);
				tbFName.Text = "";
				tbName.Text = "";
				tbOName.Text = "";
				tbAddress.Text = "";
				tbTel.Text = "";
				tbEmail.Text = "";
				tbManComment.Text = "";
				tbBirthday.Visible = true;
				dgvManInfo.CurrentCell = dgvManInfo.Rows[dgvManInfo.Rows.Count-1].Cells["Фамилия"]; //TODO
			}
			TbInputLeave(tbFName, null);
			TbInputLeave(tbName, null);
			TbInputLeave(tbOName, null);	
			TbInputLeave(tbAddress, null);
			TbInputLeave(tbTel, null);
			TbInputLeave(tbEmail, null);					             		
			TbInputLeave(tbManComment, null);	             
		}  
		
		void BtnAddIntervalClick(object sender, EventArgs e)
		{
			if (cbIntervalFIO.SelectedValue == null || cbIntervalPlace.SelectedValue == null)
			{
				lblAddIntervalStatus.Text = "Вы ввели не все данные. Добавление не произведено";
				lblAddIntervalStatus.ForeColor = Color.Red;
				return;
			}
			lblAddIntervalStatus.ForeColor = Color.Black;
			DataRow dr = ds.Tables["Intervals"].NewRow();
			dr[1] = cbIntervalFIO.SelectedValue;
			dr[2] = cbIntervalPlace.SelectedValue;
			dr[3] = dtpDateBegin.Value.ToString("yyyy-MM-dd");
			dr[4] = dtpDateEnd.Value.ToString("yyyy-MM-dd");
			dr[5] = cBoxIntervalsIsTrezv.Checked;
			dr[6] = cbIntervalType.SelectedIndex == 0;
			dr[7] = tbIntervalsComment.Text;
			ds.Tables["Intervals"].Rows.Add(dr);
			if (daIntervals.Update(ds.Tables["Intervals"]) == -1)
			{
				lblAddIntervalStatus.Text = String.Format("Добавление \"{0} с {1} по {2}\" не удалось",
				                                          cbIntervalFIO.Text, dtpDateBegin.Value.ToString("dd MMMM yyyy"),
				                                          dtpDateEnd.Value.ToString("dd MMMM yyyy"));
				dr.Delete();
			}
			else
			{
				lblAddIntervalStatus.Text = String.Format("Добавление \"{0} с {1} по {2}\" успешно",
				                                          cbIntervalFIO.Text, dtpDateBegin.Value.ToString("dd MMMM yyyy"),
				                                          dtpDateEnd.Value.ToString("dd MMMM yyyy"));
				if (cbIntervalFIO.Enabled) cbIntervalFIO.SelectedIndex = -1;
				cbIntervalPlace.SelectedIndex = -1;
				cBoxIntervalsIsTrezv.Checked = true;
				cbIntervalType.SelectedIndex = 0;
				tbIntervalsComment.Text = "";
				LoadBasic(false);
				dgvIntervals.CurrentCell = dgvIntervals.Rows[dgvIntervals.Rows.Count-1].Cells["Фамилия"];
			}
		}
		
		void BtnAddPlaceClick(object sender, EventArgs e)
		{
			if (tbPlace.ForeColor == SystemColors.WindowFrame)
			{
				lblAddPlaceStatus.Text = "Вы не ввели название места. Добавление не произведено";
				lblAddPlaceStatus.ForeColor = Color.Red;
				return;
			}
			lblAddPlaceStatus.ForeColor = Color.Black;
			DataRow dr = ds.Tables["Places"].NewRow();
			dr[1] = tbPlace.Text;
			dr[2] = tbPlaceComment.Text == "Комментарий" ? "" : tbPlaceComment.Text;
			ds.Tables["Places"].Rows.Add(dr);
			if (daPlaces.Update(ds.Tables["Places"])==-1)
				lblAddPlaceStatus.Text = String.Format("Добавление \"{0}\" не удалось", tbPlace.Text);
			else
			{
				lblAddPlaceStatus.Text = String.Format("Добавление \"{0}\" успешно", tbPlace.Text);
				tbPlace.Text = "";
				tbPlaceComment.Text = "";
				dgvPlaces.CurrentCell = dgvPlaces.Rows[dgvPlaces.Rows.Count-1].Cells["Фамилия"];
			}
			TbInputLeave(tbPlace, null);
			TbInputLeave(tbPlaceComment, null);
		}
		#endregion
		
		void DgvCellClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.ColumnIndex == -1) return;
			DataGridView dgv = sender as DataGridView;
			bool needUpd = false;
			bool needMBox = true;
			switch(dgv.Columns[e.ColumnIndex].Name)
			{
				case "Изменить":
					FormChangeRow chngFrm = new FormChangeRow(Convert.ToInt32(dgv.Tag), dgv.Rows[e.RowIndex]);
					chngFrm.ShowDialog();
					needUpd = true;
					break;
				case "Удалить":
					DataRow[] drs;
					DelRowForm frm;
					switch(Convert.ToInt32(dgv.Tag))
					{
						case 1:
							drs = ds.Tables["Intervals"].Select("idMan = " + dgv.Rows[e.RowIndex].Cells["id"].Value);
							if (drs.Length > 0)
							{
								needMBox = false;
								DataView dv = new DataView(ds.Tables["Intervals"]);
								dv.RowFilter = "idMan = " + dgv.Rows[e.RowIndex].Cells["id"].Value;
								frm = new DelRowForm(dv, 1);
								frm.ShowDialog();
								if (frm.DialogResult == DialogResult.Cancel)
									return;
								foreach (var dr in drs) {
									dr.Delete();
								}
								daIntervals.Update(ds.Tables["Intervals"]);
							}
							break;
						case 2:
							drs = ds.Tables["Intervals"].Select("idPlace = " + dgv.Rows[e.RowIndex].Cells["id"].Value);
							if (drs.Length > 0)
							{
								needMBox = false;
								DataView dv = new DataView(ds.Tables["Intervals"]);
								dv.RowFilter = "idPlace = " + dgv.Rows[e.RowIndex].Cells["id"].Value;
								frm = new DelRowForm(dv, 2);
								frm.ShowDialog();
								if (frm.DialogResult == DialogResult.Cancel)
									return;
								foreach (var dr in drs) {
									dr.Delete();
								}
								daIntervals.Update(ds.Tables["Intervals"]);
							}
							break;
					}
					if (needMBox && 
					    MessageBox.Show("Вы действительно хотите удалить эту строку?", "Удаление записи", MessageBoxButtons.YesNo,
					                    MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
						return;
					dgv.Rows.Remove(dgv.Rows[e.RowIndex]);
					needUpd = true;
					break;
			}
			if (needUpd)
			{
				this.Validate();
				switch (Convert.ToInt32(dgv.Tag))
				{
					case 1:
						daManInfo.Update(ds.Tables["ManInfo"]);
						break;
					case 2:
						daPlaces.Update(ds.Tables["Places"]);
						break;
					case 3:
						daIntervals.Update(ds.Tables["Intervals"]);
						break;
					case 4:
						daAchievements.Update(ds.Tables["Achievements"]);
						break;
				}
				LoadBasic(false);
			}
		}
		
		void BtnGetAllIntervalsClick(object sender, EventArgs e)
		{
			dgvIntervals.DataSource = dvBasic;
			cbIntervalFIO.Enabled = true;
			cbIntervalFIO.SelectedValue = -1;
			lblIntervalsStatus.Text = "Здесь отображается ФИО человека и информация о последнем его обете";
			lblIntervalsStatus.Font = new Font(lblIntervalsStatus.Font, FontStyle.Bold);
			btnGetAllIntervals.Visible = false;
			btnGetManVows.Visible = true;
			dgvIntervals.Columns["Изменить"].Visible = false;
			dgvIntervals.Columns["Удалить"].Visible = false;
			dgvIntervals.Columns["idPlace"].Visible = false;
			dgvIntervals.Columns["Фамилия"].DisplayIndex = 0;
			dgvIntervals.Columns["Имя"].DisplayIndex = 1;
			dgvIntervals.Columns["Отчество"].DisplayIndex = 2;
			dgvIntervals.Columns["Место принятия"].DisplayIndex = 3;
			dgvIntervals.Columns["Начало"].DisplayIndex = 4;
			dgvIntervals.Columns["Конец"].DisplayIndex = 5;
			dgvIntervals.Columns["Проведён трезво"].DisplayIndex = 6;
			dgvIntervals.Columns["Комментарий"].DisplayIndex = 7;
			dgvIntervals.Columns["Изменить"].DisplayIndex = 8;
			dgvIntervals.Columns["Удалить"].DisplayIndex = 9;
		}
		
		void BtnGetManVowsClick(object sender, EventArgs e)
		{
			if (dgvIntervals.CurrentRow == null) 
			{
				MessageBox.Show("Не выбрана ни одна ячейка");
				return;
			}
			object id = dgvIntervals.Rows[dgvIntervals.CurrentRow.Index/*dgvIntervals.SelectedCells[0].RowIndex*/].Cells["id"].Value;
			dvIntervals.RowFilter = "idMan = " + id.ToString();
			dgvIntervals.DataSource = dvIntervals;
			cbIntervalFIO.SelectedValue = id;
			cbIntervalFIO.Enabled = false;
			lblIntervalsStatus.Text = "Отображаются все обеты выбранного человека: "
				+ dgvIntervals.Rows[0].Cells["ФИО"].Value.ToString();
			lblIntervalsStatus.Font = new Font(lblIntervalsStatus.Font, FontStyle.Italic);
			btnGetAllIntervals.Visible = true;
			btnGetManVows.Visible = false;
			dgvIntervals.Columns["idMan"].Visible = false;
			dgvIntervals.Columns["idPlace"].Visible = false;
			dgvIntervals.Columns["Обет?"].Visible = false;
			dgvIntervals.Columns["Изменить"].Visible = true;
			dgvIntervals.Columns["Удалить"].Visible = true;
			dgvIntervals.Columns["ФИО"].DisplayIndex = 0;
			dgvIntervals.Columns["Обет/Промежуток"].DisplayIndex = 1;
			dgvIntervals.Columns["Место принятия"].DisplayIndex = 3;
			dgvIntervals.Columns["Начало"].DisplayIndex = 4;
			dgvIntervals.Columns["Конец"].DisplayIndex = 5;
			dgvIntervals.Columns["Проведён трезво"].DisplayIndex = 6;
			dgvIntervals.Columns["Комментарий"].DisplayIndex = 7;
			dgvIntervals.Columns["Изменить"].DisplayIndex = 9;
			dgvIntervals.Columns["Удалить"].DisplayIndex = 10;
			dgvIntervals.Columns["Обет/Промежуток"].Width = 102;
		}
		
		void BtnFindManInfoClick(object sender, EventArgs e)
		{
			if (dgvIntervals.CurrentRow == null) 
			{
				MessageBox.Show("Не выбрана ни одна ячейка");
				return;
			}
			tabControl1.SelectedIndex = 1;
			object v = dgvIntervals.Rows[dgvIntervals.CurrentRow.Index].Cells["id"].Value;
			dgvManInfo.ClearSelection();
			foreach (DataGridViewRow row in dgvManInfo.Rows)
			{
				if (row.Cells["id"].Value.Equals(v))
				{
					row.Selected = true;
					dgvManInfo.CurrentCell = row.Cells[3];
					return;
				}
			}
		}
		
		void BtnFindPlaceClick(object sender, EventArgs e)
		{
			if (dgvIntervals.CurrentRow == null) 
			{
				MessageBox.Show("Не выбрана ни одна ячейка");
				return;
			}
			tabControl1.SelectedIndex = 2;
			object v = dgvIntervals.Rows[dgvIntervals.CurrentRow.Index].Cells["idPlace"].Value;
			dgvPlaces.ClearSelection();
			foreach (DataGridViewRow row in dgvPlaces.Rows)
			{
				if (row.Cells["id"].Value.Equals(v))
				{
					row.Selected = true;
					dgvPlaces.CurrentCell = row.Cells[0];
					return;
				}
			}
		}
		
		void BtnBackupClick(object sender, EventArgs e)
		{
			lblBackupStatus.Text = "Подождите...";
//			if (ServType == ST.SqlServ)
//			{
//				// @"c:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\Backup\"
//				OleDbCommand comm = new OleDbCommand();
//				comm.CommandText = string.Format(backupStrArg0, tbBackupPath.Text);
//				comm.Connection = conn;
//				try
//				{
//					conn.Open();
//					comm.ExecuteNonQuery();
//					lblBackupStatus.Text = "Успешно!";
//				}
//				catch (Exception ex)
//				{
//					MessageBox.Show("Копирование не удалось. Ошибка: " + ex.Message, "Ошибка копирования", 
//					                MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
//					lblBackupStatus.Text = "Не удалось...";
//					return;
//				}
//				finally
//				{
//					conn.Close();
//				}
//			}
//			else
//			{
				try
				{
					System.IO.Directory.CreateDirectory(Path.GetDirectoryName(tbBackupPath.Text));
					System.IO.File.Copy(settings.dbPath, tbBackupPath.Text, true);
				}
				catch (Exception ex)
				{
					MessageBox.Show("Копирование не удалось. Ошибка: " + ex.Message, "Ошибка копирования", 
					                MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
					lblBackupStatus.Text = "Не удалось...";
					lblBackupStatus.ForeColor = Color.Red;
					return;
				}
//			}
			settings.BackupPath = Path.GetDirectoryName(tbBackupPath.Text);
			lblBackupStatus.Text = String.Format("Бэкап сохранён успешно в {0}", DateTime.Now.ToString("H:mm:ss"));
			lblBackupStatus.ForeColor = Color.Black;
		}
		
		void BtnToExcelIntervalsClick(object sender, EventArgs e)
		{
			// Чтобы выполнилась инструкция INSERT INTO для Excel, в таблице - шаблоне должна быть заполнена хотя бы одна первая строка
			// Причём сколько вставлять значений, столько колонок в этих строках и должно быть заполнено
			// Вместе с тем, похоже, названием колонки считается значение ячейки этой колонки в первой строке
			if (dgvIntervals.Columns["Изменить"].Visible == false) // Значит отображается Basic
			{
				SaveFileDialog SaveFD = new SaveFileDialog();
				SaveFD.DefaultExt = ".xls";
				SaveFD.AddExtension = true;
				SaveFD.FileName = "БД_для_ОТ_ПоследниеОбеты_"+ DateTime.Now.ToString("dd.MM.yyyy");
				SaveFD.Filter = "Файлы MS Excel 2003 (*.xls)|*.xls";
				if (SaveFD.ShowDialog() != DialogResult.OK)
					return;
				string filename = SaveFD.FileName; // путь файла
				String PatternPath = settings.patternPath + "Basic.xls";
				System.IO.File.Copy(PatternPath, filename, true); // копируем файл - шаблон таблицы 
				string connectionString = "provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + filename + "';Extended Properties=Excel 8.0;";
				using (OleDbConnection ole = new OleDbConnection ( connectionString )) // используем OleDb
				{
					try
					{
						ole.Open ( );
						for (int i = 0; i < dgvIntervals.Rows.Count; i++)
						{
							var cmd = new OleDbCommand ( "INSERT INTO [Лист1$]  VALUES ('" + Convert.ToString(i+1) + "','" +
							                            dgvIntervals.Rows[i].Cells["Фамилия"].Value + "','" +  dgvIntervals.Rows[i].Cells["Имя"].Value + "','" + dgvIntervals.Rows[i].Cells["Отчество"].Value + "','" +
							                            dgvIntervals.Rows[i].Cells["Место принятия"].Value + "','" +  ((DateTime)dgvIntervals.Rows[i].Cells["Начало"].Value).ToString("d MMMM yyyy")
							                            + "','" + ((DateTime)dgvIntervals.Rows[i].Cells["Конец"].Value).ToString("d MMMM yyyy") + "','" +
							                            ((bool)dgvIntervals.Rows[i].Cells["Проведён трезво"].Value == true ? "Да" : "Нет") + "','" +  dgvIntervals.Rows[i].Cells["Комментарий"].Value + "')", ole );
							cmd.ExecuteNonQuery ( ); // вставляем данные в лист1 файла - filename
							cmd.Dispose ( );
						}
					}
					finally
					{
						ole.Close ( );
					}
				}
			}
			else
			{
				string filter = dvIntervals.RowFilter;
				if (MessageBox.Show("Если вы хотите создать Excel таблицу, содержащую данные обо всех обетах и промежутках между ними," +
				                    "нажмите \"да\". Если хотите создать таблицу с данными, отображаемыми сейчас на форме, нажмите \"нет\".",
				                    "Создать таблицу с данными обо всех обетах?", MessageBoxButtons.YesNo) == DialogResult.Yes)
				{
					dvIntervals.RowFilter = "";
				}
				SaveFileDialog SaveFD = new SaveFileDialog();
				SaveFD.DefaultExt = ".xls";
				SaveFD.AddExtension = true;
				SaveFD.FileName = "БД_для_ОТ_Обеты_"+ DateTime.Now.ToString("dd.MM.yyyy");
				SaveFD.Filter = "Файлы MS Excel 2003 (*.xls)|*.xls";
				if (SaveFD.ShowDialog() != DialogResult.OK)
					return;
				string filename = SaveFD.FileName; // путь файла
				String PatternPath = settings.patternPath + @"Intervals.xls";
				System.IO.File.Copy(PatternPath, filename, true); // копируем файл - шаблон таблицы 
				string connectionString = "provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + filename + "';Extended Properties=Excel 8.0;";
				using (OleDbConnection ole = new OleDbConnection ( connectionString )) // используем OleDb
				{
					try
					{
						ole.Open ( );
						for (int i = 0; i < dgvIntervals.Rows.Count; i++)
						{
							var cmd = new OleDbCommand ( "INSERT INTO [Лист1$]  VALUES ('" + Convert.ToString(i+1) + "','" +
							                            dgvIntervals.Rows[i].Cells["ФИО"].Value + "','" + (dgvIntervals.Rows[i].Cells["Обет/Промежуток"].Value.ToString() == "Обет" ? "Обет" : "Промежуток") + "','" +
							                            dgvIntervals.Rows[i].Cells["Место принятия"].Value + "','" +  ((DateTime)dgvIntervals.Rows[i].Cells["Начало"].Value).ToString("d MMMM yyyy")
							                            + "','" + ((DateTime)dgvIntervals.Rows[i].Cells["Конец"].Value).ToString("d MMMM yyyy") + "','" +
							                            ((bool)dgvIntervals.Rows[i].Cells["Проведён трезво"].Value == true ? "Да" : "Нет") + "','" +  dgvIntervals.Rows[i].Cells["Комментарий"].Value + "')", ole );
							cmd.ExecuteNonQuery ( ); // вставляем данные в лист1 файла - filename
							cmd.Dispose ( );
						}
					}
					finally
					{
						ole.Close ( );
					}
				}
				if (dvIntervals.RowFilter == "") dvIntervals.RowFilter = filter;
			}
		}
		
		void BtnToExcelManInfoClick(object sender, EventArgs e)
		{
			SaveFileDialog SaveFD = new SaveFileDialog();
			SaveFD.DefaultExt = ".xls";
			SaveFD.AddExtension = true;
			SaveFD.FileName = "БД_для_ОТ_Инфо-О-Людях_"+ DateTime.Now.ToString("dd.MM.yyyy");
			SaveFD.Filter = "Файлы MS Excel 2003 (*.xls)|*.xls";
			if (SaveFD.ShowDialog() != DialogResult.OK)
				return;
			string filename = SaveFD.FileName; // путь файла
			String PatternPath = settings.patternPath + @"ManInfo.xls";
			System.IO.File.Copy(PatternPath, filename, true); // копируем файл - шаблон таблицы
			string connectionString = "provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + filename + "';Extended Properties=Excel 8.0;";
			using (OleDbConnection ole = new OleDbConnection ( connectionString )) // используем OleDb
			{
				try
				{
					ole.Open ( );
					for (int i = 0; i < dgvManInfo.Rows.Count; i++)
					{
						var cmd = new OleDbCommand ( "INSERT INTO [Лист1$]  VALUES ('" + Convert.ToString(i+1) + "','" +
						                            dgvManInfo.Rows[i].Cells["Фамилия"].Value + "','" +  dgvManInfo.Rows[i].Cells["Имя"].Value + "','" + dgvManInfo.Rows[i].Cells["Отчество"].Value + "','" +
						                            ((DateTime)dgvManInfo.Rows[i].Cells["Дата рождения"].Value).ToString("d MMMM yyyy") + "','" + dgvManInfo.Rows[i].Cells["Адрес"].Value
						                            + "','" + dgvManInfo.Rows[i].Cells["Телефон"].Value + "','" + dgvManInfo.Rows[i].Cells["Электр.почта"].Value
						                            + "','" +  dgvManInfo.Rows[i].Cells["Комментарий"].Value + "')", ole );
						cmd.ExecuteNonQuery ( ); // вставляем данные в лист1 файла - filename
						cmd.Dispose ( );
					}
				}
				finally
				{
					ole.Close ( );
				}
			}
		}
		
		void BtnToExcelPlacesClick(object sender, EventArgs e)
		{
			SaveFileDialog SaveFD = new SaveFileDialog();
			SaveFD.DefaultExt = ".xls";
			SaveFD.AddExtension = true;
			SaveFD.FileName = "БД_для_ОТ_Места_"+ DateTime.Now.ToString("dd.MM.yyyy");
			SaveFD.Filter = "Файлы MS Excel 2003 (*.xls)|*.xls";
			if (SaveFD.ShowDialog() != DialogResult.OK)
				return;
			string filename = SaveFD.FileName; // путь файла
			String PatternPath = settings.patternPath + @"Places.xls";
			System.IO.File.Copy(PatternPath, filename, true); // копируем файл - шаблон таблицы
			string connectionString = "provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + filename + "';Extended Properties=Excel 8.0;";
			using (OleDbConnection ole = new OleDbConnection ( connectionString )) // используем OleDb
			{
				try
				{
					ole.Open ( );
					for (int i = 0; i < dgvPlaces.Rows.Count; i++)
					{
						var cmd = new OleDbCommand ( "INSERT INTO [Лист1$]  VALUES ('" + Convert.ToString(i+1) + "','"
						                            + dgvPlaces.Rows[i].Cells["Место"].Value
						                            + "','" + dgvPlaces.Rows[i].Cells["Комментарий"].Value + "')", ole );
						cmd.ExecuteNonQuery ( ); // вставляем данные в лист1 файла - filename
						cmd.Dispose ( );
					}
				}
				finally
				{
					ole.Close ( );
				}
			}
		}
		
		void BtnBackupBrowseClick(object sender, EventArgs e)
		{
			SaveFileDialog SaveFD = new SaveFileDialog();
			SaveFD.InitialDirectory = settings.BackupPath;
			SaveFD.DefaultExt = ".accdb";
			SaveFD.AddExtension = true;
			SaveFD.FileName = "SocietyOfSobriety_backup_"+ DateTime.Now.ToString("dd.MM.yyyy");
			SaveFD.Filter = "Файлы MS Acess (*.accdb)|*.accdb| Все файлы (*.*)|*.*";
			if (SaveFD.ShowDialog() == DialogResult.OK)
			{
				tbBackupPath.Text = SaveFD.FileName;
				lblBackupStatus.Text = "Путь для сохранения выбран, нажмите \"Cохранить копию базы данных\" -> ";
				lblBackupStatus.ForeColor = Color.Magenta;
			}
		}
		
		void BtnDBPathBrowseClick(object sender, EventArgs e)
		{
			var OpenFD = new OpenFileDialog();
			OpenFD.InitialDirectory = Path.GetDirectoryName(settings.dbPath);
			OpenFD.DefaultExt = ".accdb";
			OpenFD.AddExtension = true;
			OpenFD.FileName = "SocietyOfSobriety";
			OpenFD.Filter = "Файлы MS Acess (*.accdb)|*.accdb|Все файлы (*.*)|*.*";
			if (OpenFD.ShowDialog() == DialogResult.OK)
			{
				tbDBPath.Text = OpenFD.FileName;
				lblSaveSettingsStatus.Text = "Путь до базы данных выбран, нажмите \"Cохранить\" -> ";
				lblSaveSettingsStatus.ForeColor = Color.Magenta;
			}
		}
		
		void BtnSyncPathBrowseClick(object sender, EventArgs e)
		{
			SaveFileDialog SaveFD = new SaveFileDialog();
			SaveFD.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\БД для ОТ\ОбъединённыеБД\";
			SaveFD.DefaultExt = ".accdb";
			SaveFD.AddExtension = true;
			SaveFD.FileName = "SocietyOfSobriety_Слияние_"+ DateTime.Now.ToString("dd.MM.yyyy");
			SaveFD.Filter = "Файлы MS Acess (*.accdb)|*.accdb| Все файлы (*.*)|*.*";
			if (SaveFD.ShowDialog() == DialogResult.OK)
			{
				tbSyncPath.Text = SaveFD.FileName;
				btnSyncSave.Enabled = true;
				lblSyncStatus.Text = "Всё выбрано, нажмите \"Сохранить\" ->";
				lblSyncStatus.ForeColor = Color.Magenta;
			}
		}
		
		String[] SyncFileNames;
		void BtnSyncOpenBrowseClick(object sender, EventArgs e)
		{
			OpenFileDialog OpenFD = new OpenFileDialog();
			OpenFD.InitialDirectory = @"C:\db\"; //Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			OpenFD.DefaultExt = ".accdb";
			OpenFD.AddExtension = true;
			OpenFD.Filter = "Файлы MS Acess (*.accdb)|*.accdb| Все файлы (*.*)|*.*";
			OpenFD.Multiselect = true;
			if (OpenFD.ShowDialog() == DialogResult.OK)
			{
				if (OpenFD.FileNames.Length < 2)
				{
					lblSyncFileCountStatus.Text = "Выбрано меньше двух файлов";
					return;
				}
				SyncFileNames = OpenFD.FileNames;
				lblSyncFileCountStatus.Text = String.Format("Выбрано {0} файла(ов)", SyncFileNames.Length);
				btnSyncPathBrowse.Enabled = true;
			}
		}
		
		void BtnSyncSaveClick(object sender, EventArgs e)
		{
			lblSyncStatus.Text = "Подождите...";
			
			try
			{
				DataTable dtIntervals = ds.Tables.Add("SyncIntervals");
				DataTable dtManInfo = ds.Tables.Add("SyncManInfo");
				DataTable dtPlaces = ds.Tables.Add("SyncPlaces");
				DataTable dtAddIntervals = ds.Tables.Add("SyncAddIntervals");
				DataTable dtAddManInfo = ds.Tables.Add("SyncAddManInfo");
				DataTable dtAddPlaces = ds.Tables.Add("SyncAddPlaces");
				
				OleDbDataAdapter daSyncIntervals;
				OleDbDataAdapter daSyncManInfo;
				OleDbDataAdapter daSyncPlaces;
				
				String SavingFile = tbSyncPath.Text;
				System.IO.File.Copy(SyncFileNames[0], SavingFile, true);
				OleDbConnection SyncConn = new OleDbConnection(connStr + SavingFile);
				
				daSyncIntervals = new OleDbDataAdapter("SELECT * FROM Intervals", SyncConn);
				daSyncManInfo = new OleDbDataAdapter("SELECT * FROM ManInfo", SyncConn);
				daSyncPlaces = new OleDbDataAdapter("SELECT * FROM Places", SyncConn);
				OleDbCommandBuilder ScbMI = new OleDbCommandBuilder(daSyncIntervals);
				OleDbCommandBuilder ScbP = new OleDbCommandBuilder(daSyncManInfo);
				OleDbCommandBuilder ScbI = new OleDbCommandBuilder(daSyncPlaces);
					
				OleDbCommand ins = ScbMI.GetInsertCommand();
				daSyncIntervals.Fill(dtIntervals);
				daSyncManInfo.Fill(dtManInfo);
				daSyncPlaces.Fill(dtPlaces);
				
				for(int i = 1; i<SyncFileNames.Length; i++)
				{
					dtAddIntervals.Clear();
					dtAddManInfo.Clear();
					dtAddPlaces.Clear();
					OleDbConnection SyncConnAdd = new OleDbConnection(connStr + SyncFileNames[i]);
					
					OleDbDataAdapter daSyncAddIntervals = new OleDbDataAdapter("SELECT * FROM Intervals", SyncConnAdd);
					OleDbDataAdapter daSyncAddManInfo = new OleDbDataAdapter("SELECT * FROM ManInfo", SyncConnAdd);
					OleDbDataAdapter daSyncAddPlaces = new OleDbDataAdapter("SELECT * FROM Places", SyncConnAdd);
					
					daSyncAddIntervals.Fill(dtAddIntervals);
					daSyncAddManInfo.Fill(dtAddManInfo);
					daSyncAddPlaces.Fill(dtAddPlaces);
					
					SyncConn.Open();
					Dictionary<int,int> ManInfoMap = new Dictionary<int, int>();
					for (int j = 0; j < dtAddManInfo.Rows.Count; j++)
					{
						int key = Convert.ToInt32(dtAddManInfo.Rows[j].ItemArray[0]);
						DataRow dr = dtManInfo.NewRow();
						dr[1] = dtAddManInfo.Rows[j][1];
						dr[2] = dtAddManInfo.Rows[j][2];
						dr[3] = dtAddManInfo.Rows[j][3];
						dr[4] = dtAddManInfo.Rows[j][4];
						dr[5] = dtAddManInfo.Rows[j][5];
						dr[6] = dtAddManInfo.Rows[j][6];
						dr[7] = dtAddManInfo.Rows[j][7];
						dr[8] = dtAddManInfo.Rows[j][8];
						dtManInfo.Rows.Add(dr);
						daSyncManInfo.Update(dtManInfo);
						OleDbCommand oCmd = new OleDbCommand("SELECT @@IDENTITY", SyncConn);
						ManInfoMap.Add(key, Convert.ToInt32(oCmd.ExecuteScalar()));
					}
					
					Dictionary<int,int> PlacesMap = new Dictionary<int, int>();
					for (int j = 0; j < dtAddPlaces.Rows.Count; j++)
					{
						int key = Convert.ToInt32(dtAddPlaces.Rows[j].ItemArray[0]);
						DataRow dr = dtPlaces.NewRow();
						dr[1] = dtAddPlaces.Rows[j][1];
						dr[2] = dtAddPlaces.Rows[j][2];
						dtPlaces.Rows.Add(dr);
						daSyncPlaces.Update(dtPlaces);
						OleDbCommand oCmd = new OleDbCommand("SELECT @@IDENTITY", SyncConn);
						PlacesMap.Add(key, Convert.ToInt32(oCmd.ExecuteScalar()));
					}
					
					for (int j = 0; j<dtAddIntervals.Rows.Count; j++)
					{
						DataRow dr = dtIntervals.NewRow();
						dr[1] = ManInfoMap[Convert.ToInt32(dtAddIntervals.Rows[j][1])];
						dr[2] = PlacesMap[Convert.ToInt32(dtAddIntervals.Rows[j][2])];
						dr[3] = dtAddIntervals.Rows[j][3];
						dr[4] = dtAddIntervals.Rows[j][4];
						dr[5] = dtAddIntervals.Rows[j][5];
						dr[6] = dtAddIntervals.Rows[j][6];
						dr[7] = dtAddIntervals.Rows[j][7];
						dtIntervals.Rows.Add(dr);
					}
					MessageBox.Show(Convert.ToString(daSyncIntervals.Update(dtIntervals)));
					SyncConn.Close();
				}
				
				ds.Tables.Remove("SyncIntervals");
				ds.Tables.Remove("SyncManInfo");
				ds.Tables.Remove("SyncPlaces");
				
				ds.Tables.Remove("SyncAddIntervals");
				ds.Tables.Remove("SyncAddManInfo");
				ds.Tables.Remove("SyncAddPlaces");
			}
			catch (Exception ex)
			{
				lblSyncStatus.Text = "Не удалось.";
				MessageBox.Show("Не удалось сохранить объединённую базу. Ошибка: " + ex.Message, "Ошибка объединения баз данных", 
				                MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
				return;
			}
			lblSyncStatus.ForeColor = Color.Black;
			lblSyncStatus.Text = "Успешно!";
		}
		
		bool SaveSettings()
		{
			try
			{
				using (Stream writer = new FileStream(settingsPath, FileMode.Create))
				{
					XmlSerializer serializer = new XmlSerializer(typeof(ProgSettings));
					serializer.Serialize(writer, settings);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Не удалось сохранить настройки. Ошибка: " + ex.Message, "Ошибка сохранения настроек", 
				                MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
				return false;
			}
			return true;
		}
		
		void BtnSaveSettingsClick(object sender, EventArgs e)
		{
			settings.dbPath = tbDBPath.Text;
			if (!SaveSettings()) 
			{
				lblSaveSettingsStatus.Text = String.Format("Cохранение настроек не удалось. Программа перезагружает данные ({0})", 
			                                           DateTime.Now.ToString("H:mm:ss"));
			}
			else lblSaveSettingsStatus.Text = String.Format("Настройки сохранены. Программа перезагружает данные ({0})", 
			                                           DateTime.Now.ToString("H:mm:ss"));
			lblSaveSettingsStatus.ForeColor = Color.Black;
			dgvIntervals.Columns.Clear();
			dgvManInfo.Columns.Clear();
			dgvPlaces.Columns.Clear();
			tabControl1.SelectTab(0);
			MainLoad_Wrapper();
		}
		
		void MainFormFormClosing(object sender, FormClosingEventArgs e)
		{
			SaveSettings();
		}
		
		void AddIntervalCtrlsKeyUp(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter) {
				btnAddInterval.PerformClick();
			}
		}
		
		void AddManCtrlsKeyUp(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter) {
				btnAddMan.PerformClick();
			}
		}
		
		void AddPlaceCtrlsKeyUp(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter) {
				btnAddPlace.PerformClick();
			}
		}
		
		void CbIntervalFIOLeave(object sender, EventArgs e)
		{
			if (!String.IsNullOrWhiteSpace(cbIntervalFIO.Text)) {
				cbIntervalFIO.SelectedIndex = cbIntervalFIO.FindString(cbIntervalFIO.Text);
			}
		}
	}
}
